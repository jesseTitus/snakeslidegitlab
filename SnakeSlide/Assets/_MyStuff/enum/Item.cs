using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public enum Item
    {
        Skin1,
        Skin2,
        Skin3,
        Skin4,
        Skin5,
        Skin6,
        Bg1,
        Bg2,
        Bg3,
        Bg4,
        Soundtrack1,
        Soundtrack2,
        Soundtrack3,
        Soundtrack4,
        Soundtrack5,
        Soundtrack6
    } 
}
