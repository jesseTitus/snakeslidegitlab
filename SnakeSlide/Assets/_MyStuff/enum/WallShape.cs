using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WallShape
{
    Equilateral,
    Isosceles,
    Oval,
    RightTriangle,
    Round
}
