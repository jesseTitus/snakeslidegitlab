using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class ConfigData 
{
    #region fields
    const string ConfigDataFileName = "ConfigData.csv";
    Dictionary<ConfigDataValueName, string> stringValues = new Dictionary<ConfigDataValueName, string>();
    Dictionary<ConfigDataValueName, float> floatValues = new Dictionary<ConfigDataValueName, float>();
    #endregion

    #region prop

    //public string Id { get { return stringValues[ConfigDataValueName.Id]; } }
    public string FeedbackUrl { get { return stringValues[ConfigDataValueName.FeedbackUrl]; } }

    //public float PlayerBoostSpeed { get { return floatValues[ConfigDataValueName.PlayerBoostSpeed]; } }
    //public float PlayerBoostDuration { get { return floatValues[ConfigDataValueName.PlayerBoostDuration]; } }
    //public float PlayerMaxSpeed { get { return floatValues[ConfigDataValueName.PlayerMaxSpeed]; } }
    //public float PlayerAcceleration { get { return floatValues[ConfigDataValueName.PlayerAcceleration]; } }
    //public float PlayerRotateSpeed { get { return floatValues[ConfigDataValueName.PlayerRotateSpeed]; } }
    //public float PlayerDeathAnimationLength { get { return floatValues[ConfigDataValueName.PlayerDeathAnimationLength]; } }
    //public float PlayerMaxBodyParts { get { return floatValues[ConfigDataValueName.PlayerMaxBodyParts]; } }
    public float SpawnerJumpRate { get { return floatValues[ConfigDataValueName.SpawnerJumpRate]; } }
    public float SpawnerRateGenA { get { return floatValues[ConfigDataValueName.SpawnerRateGenA]; } }
    public float SpawnerRateGenB { get { return floatValues[ConfigDataValueName.SpawnerRateGenB]; } }
    public float SpawnerRateGenC { get { return floatValues[ConfigDataValueName.SpawnerRateGenC]; } }
    public float SpawnerDifficultyInterval { get { return floatValues[ConfigDataValueName.SpawnerDifficultyInterval]; } }
    //public float BodyFramesPerRetrieval { get { return floatValues[ConfigDataValueName.BodyFramesPerRetrieval]; } }
    //public float BodySeparation { get { return floatValues[ConfigDataValueName.BodySeparation]; } }
    //public float BodyPositionAlignSpeed { get { return floatValues[ConfigDataValueName.BodyPositionAlignSpeed]; } }
    //public float BodyRotationAlignSpeed { get { return floatValues[ConfigDataValueName.BodyRotationAlignSpeed]; } }
    #endregion

    #region constructor
    public ConfigData()
    {
        StreamReader input = null;

        try
        {
            input = File.OpenText(Path.Combine(Application.streamingAssetsPath,
                ConfigDataFileName));

            // loop through csv - look for key-value pairs
            string currLine = input.ReadLine();
            while (currLine != null)
            {
                // get key, value from csv (strings)
                string[] tokens = currLine.Split(',');
                // convert value in string to actual enum value
                ConfigDataValueName valueName = (ConfigDataValueName)Enum.Parse(
                    typeof(ConfigDataValueName), tokens[0]);
                // add key, value to dict
                //stringValues.Add(valueName, float.Parse(tokens[1]));
                
                float number;
                if(float.TryParse(tokens[1], out number))
                {
                    floatValues.Add(valueName, number);
                } else
                {
                    stringValues.Add(valueName, (tokens[1]));
                }

                currLine = input.ReadLine();
            }
        }
        catch (Exception)
        {
            Debug.Log("ConfigData csv error... SetDefaultValues()");
            
            SetDefaultValues();
        }
        finally
        {
            if (input != null)
            {
                input.Close();
            }
        }
    }

    void SetDefaultValues()
    {
        stringValues.Clear();
        //stringValues.Add(ConfigDataValueName.Id, "com.Upfiregames.Upsnake");
        stringValues.Add(ConfigDataValueName.FeedbackUrl, "https://upfiregames.wordpress.com/contact/");

        floatValues.Clear();
        //floatValues.Add(ConfigDataValueName.PlayerBoostSpeed, 1.3f);
        //floatValues.Add(ConfigDataValueName.PlayerBoostDuration, 0.3f);
        //floatValues.Add(ConfigDataValueName.PlayerMaxSpeed, 5f);
        //floatValues.Add(ConfigDataValueName.PlayerAcceleration, 1f);
        //floatValues.Add(ConfigDataValueName.PlayerRotateSpeed, 5f);
        //floatValues.Add(ConfigDataValueName.PlayerDeathAnimationLength, 2f);
        //floatValues.Add(ConfigDataValueName.PlayerMaxBodyParts, 15f);
        floatValues.Add(ConfigDataValueName.SpawnerJumpRate, 30f);
        floatValues.Add(ConfigDataValueName.SpawnerRateGenA, 0.33f);
        floatValues.Add(ConfigDataValueName.SpawnerRateGenB, 0.33f);
        floatValues.Add(ConfigDataValueName.SpawnerRateGenC, 0.33f);
        floatValues.Add(ConfigDataValueName.SpawnerDifficultyInterval, 30f);
    }
    #endregion
}
