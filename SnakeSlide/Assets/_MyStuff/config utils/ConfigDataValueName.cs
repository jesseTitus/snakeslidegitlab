using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ConfigDataValueName 
{
    Id,
    FeedbackUrl,
    PlayerBoostSpeed,
    PlayerBoostDuration,
    PlayerMaxSpeed,
    PlayerAcceleration,
    PlayerRotateSpeed,
    PlayerDeathAnimationLength,
    PlayerMaxBodyParts,
    SpawnerJumpRate,
    SpawnerRateGenA,
    SpawnerRateGenB,
    SpawnerRateGenC,
    SpawnerDifficultyInterval,
    BodyFramesPerRetrieval,
    BodySeparation,
    BodyPositionAlignSpeed,
    BodyRotationAlignSpeed
}
