using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ConfigUtils 
{
    static ConfigData configData;

    #region properties
    //public static string Id { get { return configData.Id; } }
    public static string FeedbackUrl { get { return configData.FeedbackUrl; } }

    //public static float PlayerBoostSpeed { get { return configData.PlayerBoostSpeed; } }
    //public static float PlayerBoostDuration { get { return configData.PlayerBoostDuration; } }
    //public static float PlayerMaxSpeed { get { return configData.PlayerMaxSpeed; } }
    //public static float PlayerAcceleration { get { return configData.PlayerAcceleration; } }
    //public static float PlayerRotateSpeed { get { return configData.PlayerRotateSpeed; } }
    //public static float PlayerDeathAnimationLength { get { return configData.PlayerDeathAnimationLength; } }
    //public static float PlayerMaxBodyParts { get { return configData.PlayerMaxBodyParts; } }
    public static float SpawnerJumpRate { get { return configData.SpawnerJumpRate; } }
    public static float SpawnerRateGenA { get { return configData.SpawnerRateGenA; } }
    public static float SpawnerRateGenB { get { return configData.SpawnerRateGenB; } }
    public static float SpawnerRateGenC { get { return configData.SpawnerRateGenC; } }
    public static float SpawnerDifficultyInterval { get { return configData.SpawnerDifficultyInterval; } }

    //public static float BodyFramesPerRetrieval { get { return configData.BodyFramesPerRetrieval; } }
    //public static float BodySeparation { get { return configData.BodySeparation; } }
    //public static float BodyPositionAlignSpeed { get { return configData.BodyPositionAlignSpeed; } }
    //public static float BodyRotationAlignSpeed { get { return configData.BodyRotationAlignSpeed; } }
    #endregion

    public static void Initialize()
    {
        configData = new ConfigData();
    }
}
