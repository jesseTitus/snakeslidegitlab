using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class GameInitializer : MonoBehaviour
    {
        [SerializeField]
        GameObject hudCanvas = null;

        public DatabaseExtras databaseExtras;

        
        void Awake()
        {

            // Initialize scripts
            EventManager.Initialize();
            SkinDatabase.Initialize();
            DataBase.Initialize();
            ConfigUtils.Initialize();


            hudCanvas.SetActive(true);
        }
    }

}