using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// DO NOT REMOVE
/// Deletes self to ensure hole checker doesn't exist during real game
/// </summary>
public class CanvasLevelGen : MonoBehaviour
{
    void Start()
    {
        gameObject.SetActive(false);   
    }
}
