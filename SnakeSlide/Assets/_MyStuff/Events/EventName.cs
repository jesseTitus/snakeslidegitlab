using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EventName 
{
    ShowTutorialA,
    ShowTutorialB,
    EndTutorial,
    AskReview,
    Load,
    Gameover,
    Play,
    Restart,
    Rate,
    SendCoords,
    PointCollected,
    GemCollected,
    GemsPurchased,
    GemsSpent,        // inform HUD/GM, buy item, save data
    SkinPurchased,
    SkinEquipped,
    BackgroundPurchased,
    BackgroundEquipped,
    BackgroundUpdated,
    SoundtrackPurchased,
    SoundtrackEquipped,
    WallHit,
    PlayerDied,
    SendPlayerCurSpd,
    FailedPurchaseInadequateGems,
    OpenShopPurchaseConfirmation
}
