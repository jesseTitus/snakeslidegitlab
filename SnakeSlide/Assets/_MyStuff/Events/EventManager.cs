using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public static class EventManager 
{
    static Dictionary<EventName, List<IntEventInvoker>> invokers =
        new Dictionary<EventName, List<IntEventInvoker>>();
    static Dictionary<EventName, List<UnityAction<int>>> listeners =
        new Dictionary<EventName, List<UnityAction<int>>>();

    static Dictionary<EventName, List<IntEventInvoker>> tInvokers =
        new Dictionary<EventName, List<IntEventInvoker>>();
    static Dictionary<EventName, List<UnityAction<Transform>>> tListeners =
        new Dictionary<EventName, List<UnityAction<Transform>>>();

    public static void Initialize()
    {
        foreach (EventName name in Enum.GetValues(typeof(EventName)))
        {
            // create empty lists for all the dictionairy entries
            if (!invokers.ContainsKey(name))
            {
                invokers.Add(name, new List<IntEventInvoker>());
                listeners.Add(name, new List<UnityAction<int>>());
            }
            else
            {
                invokers[name].Clear();
                listeners[name].Clear();
            }

            if (!tInvokers.ContainsKey(name))
            {
                tInvokers.Add(name, new List<IntEventInvoker>());
                tListeners.Add(name, new List<UnityAction<Transform>>());
            }
            else
            {
                tInvokers[name].Clear();
                tListeners[name].Clear();
            }
        }

    }

    #region intEvents
    // adds given invoker for given event
    public static void AddInvoker(EventName eventName, IntEventInvoker invoker)
    {
        // add listeners to new invoker and add new invoker to dictionary
        foreach (var listener in listeners[eventName])
        {
            invoker.AddListener(eventName, listener);
        }
        invokers[eventName].Add(invoker);
    }
    public static void AddListener(EventName eventName, UnityAction<int> listener)
    {
        foreach (var invoker in invokers[eventName])
        {
            invoker.AddListener(eventName, listener);
        }
        listeners[eventName].Add(listener);
    }
    public static void RemoveInvoker(EventName eventName, IntEventInvoker invoker)
    {
        //remove invoker from dict
        invokers[eventName].Remove(invoker);
    }
    #endregion

    #region transformEvent

    public static void AddTInvoker(EventName eventName, IntEventInvoker invoker)
    {
        // add listeners to new invoker and add new invoker to dictionary
        foreach (var listener in tListeners[eventName])
        {
            invoker.AddTListener(eventName, listener);
        }
        tInvokers[eventName].Add(invoker);
    }
    public static void AddTListener(EventName eventName, UnityAction<Transform> listener)
    {
        foreach (var invoker in tInvokers[eventName])
        {
            invoker.AddTListener(eventName, listener);
        }
        tListeners[eventName].Add(listener);
    }
    public static void RemoveTInvoker(EventName eventName, IntEventInvoker invoker)
    {
        //remove invoker from dict
        tInvokers[eventName].Remove(invoker);
    }


    #endregion
}
