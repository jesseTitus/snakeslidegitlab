using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IntEventInvoker : MonoBehaviour
{
    protected Dictionary<EventName, UnityEvent<int>> unityEvents =
        new Dictionary<EventName, UnityEvent<int>>();


    protected Dictionary<EventName, UnityEvent<Transform>> unityTEvents =
        new Dictionary<EventName, UnityEvent<Transform>>();

    public void AddListener(EventName eventName, UnityAction<int> listener)
    {
        if (unityEvents.ContainsKey(eventName))
        {
            unityEvents[eventName].AddListener(listener);
        }
    }

    public void AddTListener(EventName eventName, UnityAction<Transform> listener)
    {
        if (unityTEvents.ContainsKey(eventName))
        {
            unityTEvents[eventName].AddListener(listener);
        }
    }
}
