using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Tilemaps;


namespace SnakeSlide
{
    public static class DataBase
    {

        public static Dictionary<TileType, TileInfo> tileDict = new Dictionary<TileType, TileInfo>();
        public static Dictionary<SoundTrack, SoundtrackInfo> soundtrackDict =
            new Dictionary<SoundTrack, SoundtrackInfo>();
        public static Dictionary<SoundEffect, AudioClip> soundEffects =
            new Dictionary<SoundEffect, AudioClip>();
        public static Dictionary<bool, Sprite> audioPrefSprites =
            new Dictionary<bool, Sprite>();
        public static Dictionary<Tuple<WallShape, TileType>, WallShapeInfo> wallShapesDict =
            new Dictionary<Tuple<WallShape, TileType>, WallShapeInfo>();

        static DatabaseExtras databaseExtras;



        public static GameObject PlayerTutCanvas;
        public static Material ShadowMaterial;
        public static Sprite StarOn;
        public static Sprite StarOff;

        public static Color shopSnakeBgColor;
        public static Color shopSoundtrackBgColor;
            

        // *Should only ever initialize once
        public static void Initialize()
        {
            // ScriptableObject that holds needed info (tile color)
            try
            {
                databaseExtras = GameObject.FindObjectOfType<GameInitializer>().databaseExtras; //Resources.Load<DatabaseExtras>("Database Extras");
            } catch
            {
                //Debug.Log("databseExtras not found!");
                DebugLogger._instance.Log("databseExtras not found!");
            }
            


            PlayerTutCanvas = databaseExtras.PlayerTutCanvas;
            ShadowMaterial = databaseExtras.ShadowMaterial;
            StarOn = databaseExtras.StarOn;
            StarOff = databaseExtras.StarOff;

            shopSnakeBgColor = databaseExtras.snakeShopBgColor;
            shopSoundtrackBgColor = databaseExtras.soundtrackBgColor;

            #region Initialize background tiles
            var sprite = new Dictionary<TileType, Sprite>();
            var shopSprite = new Dictionary<TileType, Sprite>();
            var cost = new Dictionary<TileType, int>();
            var tile = new Dictionary<TileType, Tile>();
            var color = new Dictionary<TileType, Color>();

            tile[TileType.Grass] = databaseExtras.grassTile;
            sprite[TileType.Grass] = databaseExtras.grassSprite;
            shopSprite[TileType.Grass] = databaseExtras.grassSpriteShop;
            cost[TileType.Grass] = 0;
            color[TileType.Grass] = databaseExtras.grassColor;


            tile[TileType.Sand] = databaseExtras.sandTile;
            sprite[TileType.Sand] = databaseExtras.sandSprite;
            shopSprite[TileType.Sand] = databaseExtras.sandSpriteShop;
            cost[TileType.Sand] = 15;
            color[TileType.Sand] = databaseExtras.sandColor;


            tile[TileType.WaterDark] = databaseExtras.waterDarkTile;
            sprite[TileType.WaterDark] = databaseExtras.waterDarkSprite;
            shopSprite[TileType.WaterDark] = databaseExtras.waterDarkSpriteShop;
            cost[TileType.WaterDark] = 30;
            color[TileType.WaterDark] = databaseExtras.waterDarkColor;


            tile[TileType.WaterLight] = databaseExtras.waterLightTile;
            sprite[TileType.WaterLight] = databaseExtras.waterLightSprite;
            shopSprite[TileType.WaterLight] = databaseExtras.waterLightSpriteShop;
            cost[TileType.WaterLight] = 50;
            color[TileType.WaterLight] = databaseExtras.waterLightColor;

            try
            {
                var tileNames = (Enum.GetNames(typeof(TileType)));

                foreach (var e in tileNames)
                {
                    TileType t = ((TileType)Enum.Parse(typeof(TileType), e));
                    tileDict.Add(t, new TileInfo(tile[t], sprite[t], shopSprite[t], cost[t], color[t]));
                }
            } catch
            {
                //Debug.Log("tile already in tileDict!");
                DebugLogger._instance.Log("tile already in tileDict!");
            }
            #endregion

            #region Init music assets
            var soundtrackSprite = new Dictionary<SoundTrack, Sprite>();
            var soundtrackCost = new Dictionary<SoundTrack, int>();
            var clip = new Dictionary<SoundTrack, AudioClip>();
            var musicSprite = databaseExtras.musicSprite;
            var title = new Dictionary<SoundTrack, string>();
            var soundtrackBG = databaseExtras.soundtrackBG;

            soundtrackSprite[SoundTrack.Soundtrack1] = databaseExtras.electronicCoverArt;
            soundtrackCost[SoundTrack.Soundtrack1] = 0;
            clip[SoundTrack.Soundtrack1] = databaseExtras.soundtrack1;
            title[SoundTrack.Soundtrack1] = databaseExtras.soundtrack1Title;

            soundtrackSprite[SoundTrack.Soundtrack2] = databaseExtras.deathMetalCoverArt;
            soundtrackCost[SoundTrack.Soundtrack2] = 15;
            clip[SoundTrack.Soundtrack2] = databaseExtras.soundtrack2;
            title[SoundTrack.Soundtrack2] = databaseExtras.soundtrack2Title;

            soundtrackSprite[SoundTrack.Soundtrack3] = databaseExtras.funkCoverArt;
            soundtrackCost[SoundTrack.Soundtrack3] = 30;
            clip[SoundTrack.Soundtrack3] = databaseExtras.soundtrack3;
            title[SoundTrack.Soundtrack3] = databaseExtras.soundtrack3Title;

            soundtrackSprite[SoundTrack.Soundtrack4] = databaseExtras.ominousCoverArt;
            soundtrackCost[SoundTrack.Soundtrack4] = 45;
            clip[SoundTrack.Soundtrack4] = databaseExtras.soundtrack4;
            title[SoundTrack.Soundtrack4] = databaseExtras.soundtrack4Title;

            soundtrackSprite[SoundTrack.Soundtrack5] = databaseExtras.castleMusicCoverArt;
            soundtrackCost[SoundTrack.Soundtrack5] = 75;
            clip[SoundTrack.Soundtrack5] = databaseExtras.soundtrack5;
            title[SoundTrack.Soundtrack5] = databaseExtras.soundtrack5Title;

            soundtrackSprite[SoundTrack.Soundtrack6] = databaseExtras.creepyEchoChamberCoverArt;
            soundtrackCost[SoundTrack.Soundtrack6] = 125;
            clip[SoundTrack.Soundtrack6] = databaseExtras.soundtrack6;
            title[SoundTrack.Soundtrack6] = databaseExtras.soundtrack6Title;

            try
            {
                var soundtracks = (Enum.GetNames(typeof(SoundTrack)));

                foreach (var e in soundtracks)
                {
                    SoundTrack s = ((SoundTrack)Enum.Parse(typeof(SoundTrack), e));
                    soundtrackDict.Add(s, new SoundtrackInfo(soundtrackSprite[s], soundtrackCost[s],
                        clip[s], title[s], soundtrackBG));
                }
            }
            catch
            {
                //Debug.Log("soundtrack already in soundtrackDict!");
                DebugLogger._instance.Log("soundtrack already in soundtrackDict!");
            }
            #endregion

            #region Init sound effects
            soundEffects[SoundEffect.Die] = databaseExtras.playerDied; 
            soundEffects[SoundEffect.Gem] = databaseExtras.gemCollected;
            soundEffects[SoundEffect.Point] = databaseExtras.pointCollected;
            soundEffects[SoundEffect.Pop] = databaseExtras.pop;
            soundEffects[SoundEffect.ShopSelect] = databaseExtras.shopItemSelected;
            soundEffects[SoundEffect.ShopPurchase] = databaseExtras.shopItemPurchased;
            soundEffects[SoundEffect.PositiveSound] = databaseExtras.positiveSound;
            #endregion


            audioPrefSprites[true] = databaseExtras.soundOn;
            audioPrefSprites[false] = databaseExtras.soundOff;


            #region Wall Shape Info (sprite, pattern)


            InitializeWallShapesDict(WallShape.Equilateral,
                                    databaseExtras.equilateral1,
                                    databaseExtras.equilateral2,
                                    databaseExtras.equilateral3,
                                    databaseExtras.equilateral4);

            InitializeWallShapesDict(WallShape.Isosceles,
                                    databaseExtras.isosceles1,
                                    databaseExtras.isosceles2,
                                    databaseExtras.isosceles3,
                                    databaseExtras.isosceles4);

            InitializeWallShapesDict(WallShape.Oval,
                                    databaseExtras.oval1,
                                    databaseExtras.oval2,
                                    databaseExtras.oval3,
                                    databaseExtras.oval4);

            InitializeWallShapesDict(WallShape.RightTriangle,
                                    databaseExtras.right_triangle1,
                                    databaseExtras.right_triangle2,
                                    databaseExtras.right_triangle3,
                                    databaseExtras.right_triangle4);


            InitializeWallShapesDict(WallShape.Round,
                                    databaseExtras.round1,
                                    databaseExtras.round2,
                                    databaseExtras.round3,
                                    databaseExtras.round4);

            #endregion
        }

        static void InitializeWallShapesDict(
                                    WallShape wallShape,
                                    Sprite waterDark,
                                    Sprite grass,
                                    Sprite sand,
                                    Sprite waterLight)
        {
            var grassTuple = new Tuple<WallShape, TileType>(wallShape, TileType.Grass);
            var waterDarkTuple = new Tuple<WallShape, TileType>(wallShape, TileType.WaterDark);
            var sandTuple = new Tuple<WallShape, TileType>(wallShape, TileType.Sand);
            var waterLightTuple = new Tuple<WallShape, TileType>(wallShape, TileType.WaterLight);

            wallShapesDict[grassTuple] = new WallShapeInfo(grass);
            wallShapesDict[waterDarkTuple] = new WallShapeInfo(waterDark);
            wallShapesDict[sandTuple] = new WallShapeInfo(sand);
            wallShapesDict[waterLightTuple] = new WallShapeInfo(waterLight);

        }
    }


    public struct TileInfo
    {
        public TileInfo(Tile tile, Sprite sprite, Sprite shopSprite, int cost, Color color)
        {
            this.TileAsset = tile;
            this.Sprite = sprite;
            this.ShopSprite = shopSprite;
            this.Cost = cost;
            this.TileColor = color;
        }

        public Tile TileAsset { get; private set; }
        public Sprite Sprite { get; private set; }
        public Sprite ShopSprite { get; private set; }
        public int Cost { get; private set; }
        public Color TileColor { get; private set; }
    }

    public struct SoundtrackInfo
    {
        public SoundtrackInfo(Sprite sprite, int cost, AudioClip clip, string title, Sprite bg)
        {
            this.Sprite = sprite;
            this.Cost = cost;
            this.Clip = clip;
            this.Title = title;
            this.Bg = bg;
        }

        public Sprite Sprite { get; private set; }
        public int Cost { get; private set; }
        public AudioClip Clip { get; private set; }
        public string Title { get; private set; }
        public Sprite Bg { get; private set; }
    }

    public struct WallShapeInfo
    {
        public WallShapeInfo(Sprite ShapeSprite)
        {
            this.ShapeSprite = ShapeSprite;
        }

        public Sprite ShapeSprite { get; private set; }
    }
}
