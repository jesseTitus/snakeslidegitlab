using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    [CreateAssetMenu]
    public class ColorPallette : ScriptableObject
    {
        public Color[] Pallette;
    }

}