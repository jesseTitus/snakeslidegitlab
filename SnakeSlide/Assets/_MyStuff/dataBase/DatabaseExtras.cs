using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class DatabaseExtras : ScriptableObject
{

    public GameObject PlayerTutCanvas;
    public Material ShadowMaterial;
    public Sprite StarOn;
    public Sprite StarOff;

    #region player assets
    [Header("Snake shop bg")]
    public Sprite snakeShopBg;

    [Header("Snake shop bg Color")]
    public Color snakeShopBgColor;

    [Header("Snake Head Skin")]
    public Sprite snakeHead0;
    public Sprite snakeHead1;
    public Sprite snakeHead2;
    public Sprite snakeHead3;
    public Sprite snakeHead4;
    public Sprite snakeHead5;

    [Header("Snake Body Skin")]
    public Sprite snakeBody0;
    public Sprite snakeBody1;
    public Sprite snakeBody2;
    public Sprite snakeBody3;
    public Sprite snakeBody4;
    public Sprite snakeBody5;

    [Header("Snake Tail Skin")]
    public Sprite snakeTail0;
    public Sprite snakeTail1;
    public Sprite snakeTail2;
    public Sprite snakeTail3;
    public Sprite snakeTail4;
    public Sprite snakeTail5;

    #endregion

    [Space(20)]
    #region background assets

    [Header("Tile Assets")]
    public Tile grassTile;
    public Tile sandTile;
    public Tile waterDarkTile;
    public Tile waterLightTile;

    [Header("Tile sprites")]
    public Sprite grassSprite;
    public Sprite sandSprite;
    public Sprite waterDarkSprite;
    public Sprite waterLightSprite;

    [Header("Tile Shop sprites")]
    public Sprite grassSpriteShop;
    public Sprite sandSpriteShop;
    public Sprite waterDarkSpriteShop;
    public Sprite waterLightSpriteShop;

    [Header("Corresponding Background color")]
    public Color grassColor;
    public Color sandColor;
    public Color waterDarkColor;
    public Color waterLightColor;

    #endregion
    
    [Space(20)]

    #region sound assets
    [Header("Soundtracks")]
    public AudioClip soundtrack1;
    public AudioClip soundtrack2;
    public AudioClip soundtrack3;
    public AudioClip soundtrack4;
    public AudioClip soundtrack5;
    public AudioClip soundtrack6;

    [Header("Soundtrack Art")]
    public Sprite deathMetalCoverArt;
    public Sprite funkCoverArt;
    public Sprite electronicCoverArt;
    public Sprite ominousCoverArt;
    public Sprite castleMusicCoverArt;
    public Sprite creepyEchoChamberCoverArt;

    [Header("Soundtrack bg")]
    public Sprite soundtrackBG;

    [Header("Soundtrack bg color")]
    public Color soundtrackBgColor;

    [Header("Soundtrack Title")]
    public string soundtrack1Title;
    public string soundtrack2Title;
    public string soundtrack3Title;
    public string soundtrack4Title;
    public string soundtrack5Title;
    public string soundtrack6Title;

    [Header("Sound FX")]
    public AudioClip gemCollected;
    public AudioClip pointCollected;
    public AudioClip playerDied;
    public AudioClip pop;
    public AudioClip shopItemSelected;
    public AudioClip shopItemPurchased;
    public AudioClip positiveSound;

    [Header("Audiopref sprites")]
    public Sprite soundOn;
    public Sprite soundOff;

    [Header("Soundtrack sprites")]
    public Sprite musicSprite;
    #endregion
    [Space(20)]


    #region wall shapes
    [Header("Wall Shape Assets")]
    public Sprite equilateral1;
    public Sprite equilateral2;
    public Sprite equilateral3;
    public Sprite equilateral4;
    [Space(20)]
    public Sprite isosceles1;
    public Sprite isosceles2;
    public Sprite isosceles3;
    public Sprite isosceles4;
    [Space(20)]
    public Sprite oval1;
    public Sprite oval2;
    public Sprite oval3;
    public Sprite oval4;
    [Space(20)]
    public Sprite right_triangle1;
    public Sprite right_triangle2;
    public Sprite right_triangle3;
    public Sprite right_triangle4;
    [Space(20)]
    public Sprite round1;
    public Sprite round2;
    public Sprite round3;
    public Sprite round4;

    #endregion
}
