﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace SnakeSlide
{
    /// <summary>
    /// Track bgs and skin (owned and equipped)
    /// </summary>
    public static class SkinDatabase
    {
        public static Dictionary<Skin, SkinInfo> skinDictionary = new Dictionary<Skin, SkinInfo>();

        static DatabaseExtras databaseExtras;
        public static Sprite snakeShopBg;

        // *Should only need to ever initialise ONCE 
        public static void Initialize()
        {
            // ScriptableObject that holds needed info (tile color)
            try
            {
                databaseExtras = GameObject.FindObjectOfType<GameInitializer>().databaseExtras; //Resources.Load<DatabaseExtras>("Database Extras");
            }
            catch
            {
                //Debug.Log("databseExtras not found!");
                DebugLogger._instance.Log("databseExtras not found!");
            }
            // helper variables for setting up skin database
            //var head = new Dictionary<Skin, Sprite>();
            //var body = new Dictionary<Skin, Sprite>();
            //var cost = new Dictionary<Skin, int>();


            //head[Skin.Original] = databaseExtras.snakeHead0;
            //body[Skin.Original] = databaseExtras.snakeBody0;
            //cost[Skin.Original] = 0;

            //head[Skin.Maple] = databaseExtras.snakeHead1;
            //body[Skin.Maple] = databaseExtras.snakeBody1;
            //cost[Skin.Maple] = 15;

            //head[Skin.TreeViper] = databaseExtras.snakeHead2;
            //body[Skin.TreeViper] = databaseExtras.snakeBody2;
            //cost[Skin.TreeViper] = 30;

            //head[Skin.Cobra] = databaseExtras.snakeHead3;
            //body[Skin.Cobra] = databaseExtras.snakeBody3;
            //cost[Skin.Cobra] = 100;

            //head[Skin.Rainbow] = databaseExtras.snakeHead4;
            //body[Skin.Rainbow] = databaseExtras.snakeBody4;
            //cost[Skin.Rainbow] = 250;

            //head[Skin.BlackMamba] = databaseExtras.snakeHead5;
            //body[Skin.BlackMamba] = databaseExtras.snakeBody5;
            //cost[Skin.BlackMamba] = 500;
            
            //try
            //{
            //    var skinNames = (Enum.GetNames(typeof(Skin)));

            //    foreach (var e in skinNames) {
            //        Skin s = ((Skin)Enum.Parse(typeof(Skin), e));

            //        skinDictionary.Add(s, new SkinInfo(head[s], body[s], cost[s]));
            //    }
            //} catch
            //{
            //    Debug.Log("skin already in skinDictionary");
            //}

            try
            {
                snakeShopBg = databaseExtras.snakeShopBg;

                skinDictionary[Skin.Original] = new SkinInfo(databaseExtras.snakeHead0, databaseExtras.snakeBody0, databaseExtras.snakeTail0, 0);
                skinDictionary[Skin.Maple] = new SkinInfo(databaseExtras.snakeHead1, databaseExtras.snakeBody1, databaseExtras.snakeTail0, 15);
                skinDictionary[Skin.TreeViper] = new SkinInfo(databaseExtras.snakeHead2, databaseExtras.snakeBody2, databaseExtras.snakeTail0, 30);
                skinDictionary[Skin.Cobra] = new SkinInfo(databaseExtras.snakeHead3, databaseExtras.snakeBody3, databaseExtras.snakeTail0, 100);
                skinDictionary[Skin.Rainbow] = new SkinInfo(databaseExtras.snakeHead4, databaseExtras.snakeBody4, databaseExtras.snakeTail0, 250);
                skinDictionary[Skin.BlackMamba] = new SkinInfo(databaseExtras.snakeHead5, databaseExtras.snakeBody5, databaseExtras.snakeTail0, 500);
            } catch
            {
                //Debug.Log("skins already init.");
                DebugLogger._instance.Log("skins already init.");
            }
        }
    }

    // use to get headSprite when assigning to player
    public struct SkinInfo
    {
        public SkinInfo(Sprite headSprite, Sprite bodySprite, Sprite tailSprite, int gemCost)
        {
            this.Head = headSprite;
            this.Body = bodySprite;
            this.Tail = tailSprite;
            this.Cost = gemCost;
        }

        public Sprite Head { get; private set; }

        public Sprite Body { get; private set; }

        public Sprite Tail { get; private set; }

        public int Cost { get; private set; }
        
    }
}