using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMouseFollow : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var vertical = Input.GetAxis("Vertical");
        if (vertical != 0)
        {
            transform.position += Vector3.up * vertical * Time.deltaTime;
        }
    }
}
