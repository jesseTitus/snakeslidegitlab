using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestScrollY : MonoBehaviour
{
    public Transform player;
    Image img;
    //Material bgMaterial;
    float posPrev;
    float pos;

    Vector2 savedOffset;

    void Start()
    {
        img = GetComponent<Image>();
        //var rawImg = GetComponent<RawImage>();
        //bgMaterial = img? img.material: rawImg? rawImg.material: null;

    }

    // Update is called once per frame
    void Update()
    {
        if (player && img != null)
        {
            if (pos == 0)  //first frame
            {
                pos = player.position.y;
            } else
            {
                posPrev = pos;
                pos = player.position.y;
            }
            var offset = pos - posPrev;
            GetComponent<Image>().material.mainTextureOffset += Vector2.up * offset;
            //bgMaterial.SetTextureOffset("_MainTex",Time. * Vector2.up /** offset*/);
        }
    }
}
