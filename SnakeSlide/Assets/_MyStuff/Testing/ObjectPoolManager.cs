using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public static class ObjectPoolManager
    {
        #region fields
        static Dictionary<Difficulty, List<GameObject>> wallPool = null;
        #endregion

        public static void Initialize()
        {
        }

        #region WallPool
        public static void ReturnWallPrefabToPool(GameObject wallObj)
        {
            wallObj.SetActive(false);
            var wall = wallObj.GetComponent<Wall>();
            //wall.Reset();    //TODO - (reset points/gems)
            wallPool[Difficulty.Easy].Add(wallObj);
        }

        //public static GameObject GetWallFromPool(Difficulty difficulty)
        //{
        //    var pool = wallPool[difficulty];
        //    if (pool.Length > 0)
        //    {
        //        var w = pool[Random.Range(0, pool.Length - 1)];
        //        return w;
        //    }

        //}
        #endregion
    } 
}
