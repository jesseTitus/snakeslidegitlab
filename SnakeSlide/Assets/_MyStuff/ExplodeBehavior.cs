using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 1. Instantiate at desired pos
/// 2. Initialise
/// 3. Set off (Explode)
/// 4. Destroy self
/// </summary>
public class ExplodeBehavior : MonoBehaviour
{
    [Header("References")]
    public GameObject explodeVisuals;   // Head or Gem mesh
    public GameObject collectedParticleSystem;


    float durationOfCollectedParticleSystem;

    void Start()
    {
        durationOfCollectedParticleSystem = collectedParticleSystem.GetComponent<ParticleSystem>().main.duration;
        //explodeVisuals.GetComponent<ParticleSystem
    }

    /// <summary>
    /// Creator of this instance implants itself as parent
    ///  Child (this) can then callback to parent for behavior change
    /// </summary>
    /// <param name="explodeVisuals"></param>
    //public void Initialise(GameObject explodeVisuals)
    //{
    // *not currently using
    //    this.explodeVisuals = explodeVisuals;
    //}
    

    public void Explode()
    {
        collectedParticleSystem.transform.position = explodeVisuals.transform.position;
        explodeVisuals.SetActive(false);            //dissapear
        collectedParticleSystem.SetActive(true);    //explode
        Invoke("DeactivateGemGameObject", durationOfCollectedParticleSystem);
        
    }

    void DeactivateGemGameObject()
    {
        //gameObject.SetActive(false);
        Destroy(this);
    }
}
