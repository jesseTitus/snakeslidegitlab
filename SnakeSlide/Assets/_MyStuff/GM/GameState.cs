using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Load,
    Gameover,
    Restart,
    Play
}
