﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
//using System.Security.Policy;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

namespace SnakeSlide
{
    public class GM : IntEventInvoker
    {
        #region state


        GameState _gamestate;
        public GameState State
        {
            get { return _gamestate; }
            set
            {
                _gamestate = value;
                switch (value)
                {
                    case GameState.Load:
                        unityEvents[EventName.Load].Invoke(-1);
                        State = GameState.Play;
                        break;

                    case GameState.Gameover:
                        unityEvents[EventName.Gameover].Invoke(-1);
                        break;

                    case GameState.Restart:
                        SceneManager.LoadScene("SnakeSlide-Game"/*SceneManager.GetActiveScene().name*/);
                        break;

                    case GameState.Play:
                        unityEvents[EventName.Play].Invoke(-1);
                        StartGems = gems;
                        break;
                }

            }
        }

        #endregion

        #region fields
        [SerializeField]
        GameObject debugCanvas = null;
        [SerializeField]
        bool debugMode = false;
        int points = 0;


        #endregion

        #region Singleton
        public static GM _instance;
        #endregion

        #region properties
        public int Points { get { return points; } }
        public int Highscore { get { return highScore; } }
        public int Gems { get { return gems; } }
        public int StartGems { get; private set; }               //num of gems at start of game (use to show old score + new)
        public Skin SkinEquipped { get { return skinEquipped;}} 
        public IList<Skin> SkinsOwned { get { return skinsOwned;}}
        public TileType BackgroundEquipped { get { return backgroundEquipped; } }
        public IList<TileType> BackgroundsOwned { get { return backgroundsOwned; } }
        public SoundTrack SoundtrackEquipped { get { return soundtrackEquipped; } }
        public IList<SoundTrack> SoundtracksOwned { get { return soundtracksOwned; } }
        #endregion

        #region SAVE-DATA
        Skin skinEquipped = Skin.Original;   
        List<Skin> skinsOwned = new List<Skin>() { Skin.Original };
        TileType backgroundEquipped;
        List<TileType> backgroundsOwned = new List<TileType>() { TileType.Grass };
        SoundTrack soundtrackEquipped;
        List<SoundTrack> soundtracksOwned = new List<SoundTrack>() { SoundTrack.Soundtrack1};
        int highScore = 0;
        int gems = 0;
        bool askReview = true;
        #endregion

        [SerializeField]
        bool deleteSaveOnAwake;

        void Awake()
        {
            #region singleton
            if (_instance == null)
            {
                _instance = this;
            }
            else if (_instance != this)
            {
                Destroy(this);
            }
            #endregion

            if (deleteSaveOnAwake)
            {
                DeleteSave();
            }
            Load();

            #region events
            // Events - listeners
            EventManager.AddListener(EventName.PointCollected, OnPointCollectedEvent);
            EventManager.AddListener(EventName.GemCollected, OnGemCollectedEvent);
            EventManager.AddListener(EventName.GemsPurchased, OnGemsPurchasedEvent);
            EventManager.AddListener(EventName.PlayerDied, OnPlayerDied);
            EventManager.AddListener(EventName.SkinPurchased, OnSkinPurchased);
            EventManager.AddListener(EventName.SkinEquipped, OnSkinEquipped);
            EventManager.AddListener(EventName.BackgroundPurchased, OnBackgroundPurchased);
            EventManager.AddListener(EventName.BackgroundEquipped, OnBackgroundEquipped);
            EventManager.AddListener(EventName.SoundtrackPurchased, OnSoundtrackPurchased);
            EventManager.AddListener(EventName.SoundtrackEquipped, OnSoundtrackEquipped);

            // Events - invokers
            unityEvents[EventName.Load] = new LoadStateEvent();
            EventManager.AddInvoker(EventName.Load, this);
            unityEvents[EventName.Gameover] = new GameoverEvent();
            EventManager.AddInvoker(EventName.Gameover, this);
            unityEvents[EventName.Play] = new PlayStateEvent();
            EventManager.AddInvoker(EventName.Play, this);
            unityEvents[EventName.Restart] = new RestartStateEvent();
            EventManager.AddInvoker(EventName.Restart, this);
            unityEvents[EventName.Rate] = new RateUsEvent();
            EventManager.AddInvoker(EventName.Rate, this);
            unityEvents[EventName.GemsSpent] = new GemsSpentEvent();
            EventManager.AddInvoker(EventName.GemsSpent, this);
            #endregion
        }



        void Start()
        {
            State = GameState.Load;
        }
        void Update()
        {

            #region debug controls
            debugCanvas.SetActive(debugMode);
            if (debugMode)
            {
                if (Input.GetKeyDown(KeyCode.Keypad0))
                {

                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
                else if (Input.GetKeyDown(KeyCode.Keypad1))
                {
                    gems += 100;
                    Save();
                } else if (Input.GetKeyDown(KeyCode.Keypad2))
                {
                    debugMode = false;
                } else if (Input.GetKeyDown(KeyCode.Keypad3))
                {
                    deleteSaveOnAwake = false;
                } else if (Input.GetKeyDown(KeyCode.Keypad4))
                {
                    DeleteSave();
                }
            }
            #endregion
        }

        #region Methods


        public bool SpendIfCanAfford(int cost)
        {
            if (gems < cost)
            {
                return false;
            }

            gems -= cost;
            return true;
        }

        #endregion

        #region Events

        void OnPointCollectedEvent(int pointsCollected)
        {
            if (++points > highScore)
            {
                highScore = points;
                Save();
            }
        }

        void OnGemCollectedEvent(int unused)
        {
            AudioManager.PlaySound(SoundEffect.Gem);
            gems++;
            Save();
            if (ScreenShake._instance)
            {
                ScreenShake._instance.ShakeCamera(0.25f);
            }
        }

        void OnGemsPurchasedEvent(int gemsPurchased)
        {
            gems += gemsPurchased;
            Save();
        }

        void OnPlayerDied(int unused)
        {
            State = GameState.Gameover;
        }

        void OnSkinPurchased(int skin)
        {
            skinsOwned.Add((Skin)skin);
            unityEvents[EventName.GemsSpent].Invoke(-1);
        }

        void OnSkinEquipped(int skin)
        {
            skinEquipped = (Skin)skin;
            Save();
        }

        void OnBackgroundPurchased(int tileType)
        {
            backgroundsOwned.Add((TileType)tileType);
            unityEvents[EventName.GemsSpent].Invoke(-1);
            Save();
        }

        void OnBackgroundEquipped(int tileType)
        {
            backgroundEquipped = (TileType)tileType;
            Save();
        }

        void OnSoundtrackPurchased(int soundtrack)
        {
            soundtracksOwned.Add((SoundTrack)soundtrack);
            unityEvents[EventName.GemsSpent].Invoke(-1);
            Save();
        }

        void OnSoundtrackEquipped(int soundtrack)
        {
            soundtrackEquipped = (SoundTrack)soundtrack;
            Save();
        }


        #endregion

        #region SAVE/LOAD

        public void Save()
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

            PlayerData data = new PlayerData(highScore, 
                                            gems, 
                                            askReview, 
                                            skinsOwned,
                                            skinEquipped,
                                            backgroundsOwned,
                                            backgroundEquipped,
                                            soundtracksOwned,
                                            soundtrackEquipped);

            // take our playerdata and save it to "file"
            bf.Serialize(file, data);
            file.Close();
        }

        public void Load()
        {
            // get highscore, sound preference, gems
            if (!File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
            {
                Save();
            }

            //Debug.Log(Application.persistentDataPath + "/playerInfo.dat");
            DebugLogger._instance.Log("Save location: " + Application.persistentDataPath + "/playerInfo.dat");
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData data = (PlayerData) bf.Deserialize(file);
            file.Close();

            // Setup data
            highScore = data.highscore;
            gems = data.gems;
            skinsOwned = data.skinsOwned;
            skinEquipped = data.skinEquipped;
            backgroundsOwned = data.backgroundsOwned;
            backgroundEquipped = data.backgroundEquipped;
            soundtrackEquipped = data.soundtrackEquipped;
            soundtracksOwned = data.soundtracksOwned;
        }

        void DeleteSave()
        {
            if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
            {
                File.Delete(Application.persistentDataPath + "/playerInfo.dat");
            }
            PlayerPrefs.DeleteAll();
        }
        #endregion
    }

    [Serializable] // serializable: can now save this to a file (no monobehavior's allowed!)
    class PlayerData
    {
        public int highscore;
        public int gems;
        public bool askReview;
        public List<Skin> skinsOwned;
        public Skin skinEquipped;
        public List<TileType> backgroundsOwned;
        public TileType backgroundEquipped;
        public List<SoundTrack> soundtracksOwned;
        public SoundTrack soundtrackEquipped;

        public PlayerData(
            int highscore,
            int gems,
            bool askReview,
            List<Skin> skinsOwned,
            Skin skinEquipped,
            List<TileType> backgroundsOwned,
            TileType backgroundEquipped,
            List<SoundTrack> soundtracksOwned,
            SoundTrack soundtrackEquipped)
        {
            this.highscore = highscore;
            this.gems = gems;
            this.askReview = askReview;
            this.skinsOwned = skinsOwned;
            this.skinEquipped = skinEquipped;
            this.backgroundsOwned = backgroundsOwned;
            this.backgroundEquipped = backgroundEquipped;
            this.soundtracksOwned = soundtracksOwned;
            this.soundtrackEquipped = soundtrackEquipped;
        }
    }
}