﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    /// <summary>
    /// For tracking the path all parts follow
    /// </summary>
    public class TransformationBuffer : MonoBehaviour
    {

        public static TransformationBuffer _instance;

        #region fields
        [SerializeField]
        bool drawDebugPath = false;
        int framesTracked = 100;
        List<Transformation> _transformations = new List<Transformation>();
        #endregion

        #region properties
        public List<Transformation> Transformations { get { return _transformations; } }
        public int Length { get { return Transformations.Count; } }
        #endregion

        #region objects
        [SerializeField]
        Transform _trackedObject = null;
        #endregion

        // Adding newest at end of buffer
        // deleting oldest if buffer full
        public void AddTransformation(Transformation t)
        {
            _transformations.Add(t);
            if (_transformations.Count >= framesTracked)
            {
                _transformations.RemoveAt(0);
            }
        }

        public Transformation GetTransformation(int index)
        {
            //Debug.Log(string.Format("Count: {0}, index: {1}", _transformations.Count, index));
            return _transformations[_transformations.Count - 1 - (index)];
        }

        #region singleton (not in use)
        //void Awake()
        //{
        //    if (!_instance)
        //    {
        //        _instance = this;
        //    }
        //    else if (_instance != this)
        //    {
        //        Destroy(this);
        //    }
        //}
        #endregion

        void Start()
        {
#if UNITY_EDITOR
            if (drawDebugPath)
            {
                StartCoroutine("DrawDebugPath");
            }

#endif
        }
        
        IEnumerator DrawDebugPath()
        {
            while (true)
            {
                for (var i = 1; i < _transformations.Count; i += 2)
                {
                    Debug.DrawLine(_transformations[i - 1].position, _transformations[i].position, Color.red);
                }
                yield return null;
            }
        }

        // Store a circular buffer of Transformations (position/rotation) that creat a path of waypoints
        void LateUpdate()
        {
            if (_trackedObject == null)
            {
                return;
            }


            Transformation t = new Transformation(_trackedObject.rotation, _trackedObject.position, _trackedObject.up);
            AddTransformation(t);
        }
    }
}
