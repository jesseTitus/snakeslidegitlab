using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class KeepTextOnGameObj : MonoBehaviour
{
    [SerializeField]
    Transform followTransform;

    float offsetY;

    void Start()
    {
        followTransform = transform.parent;
        offsetY = followTransform.position.y - transform.position.y;
    }


    void Update()
    {
        transform.position = new Vector3(followTransform.position.x, 
                                        followTransform.position.y - offsetY);
    }
}
