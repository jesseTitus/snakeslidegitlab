using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class BigSkinButton : SkinButton
    {
        protected override bool IsConfirmationButton { get { return true; } }

        protected override void UpdateVisuals()
        {
            costText.text = ItemCost.ToString();
        }
    }

}