using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class BigSoundtrackButton : SoundtrackShopButton
    {

        protected override bool IsConfirmationButton { get { return true; } }

        protected override void UpdateVisuals()
        {
            costText.text = ItemCost.ToString();
        }
    }
}
