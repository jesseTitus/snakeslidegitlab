using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace SnakeSlide
{
    public class SoundtrackShopButton : ShopItem
    {
        public SoundTrack mySoundtrack;

        #region implement abstract properties
        
        protected override Sprite ItemBg { get { return DataBase.soundtrackDict[mySoundtrack].Bg; } }
        protected override int ItemCost { get { return DataBase.soundtrackDict[mySoundtrack].Cost; } }
        protected override int ItemNumber
        {
            get { return (int)mySoundtrack + (int)Item.Soundtrack1; }
        }
        protected override bool IsItemOwned
        {
            get { return GM._instance.SoundtracksOwned.Contains(mySoundtrack); }
        }
        protected override bool IsItemEquipped
        {
            get { return GM._instance.SoundtrackEquipped == mySoundtrack; }
        }
        protected override Color BgColor { get { return DataBase.shopSoundtrackBgColor; } }
        #endregion

        #region properties
        //protected override Image.Type IconImageType { get { return Image.Type.Tiled; } }
        protected override string TitleText { get { return DataBase.soundtrackDict[mySoundtrack].Title; } }
        #endregion

        public override void Start()
        {
            //bgColor = DataBase.shopSoundtrackBgColor;

            base.Start();


            //invoke
            unityEvents[EventName.SoundtrackEquipped] = new SoundtrackEquippedEvent();
            unityEvents[EventName.SoundtrackPurchased] = new SoundtrackPurchasedEvent();
            EventManager.AddInvoker(EventName.SoundtrackEquipped, this);
            EventManager.AddInvoker(EventName.SoundtrackPurchased, this);

            //listen
            EventManager.AddListener(EventName.SoundtrackEquipped, OnSoundtrackChanged);
            
            UpdateVisuals();
            initialized = true;

        }


        #region methods

        void OnSoundtrackChanged(int unused)
        {
            UpdateVisuals();
        }
        

        protected override void AttemptPurchase()
        {
            unityEvents[EventName.SoundtrackPurchased].Invoke((int)mySoundtrack);
        }

        protected override void EquipSelectedItem()
        {
            unityEvents[EventName.SoundtrackEquipped].Invoke((int)mySoundtrack);
        }
        #endregion
    }
}
