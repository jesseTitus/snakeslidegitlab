using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SnakeSlide
{

    public class ResetShopScrollAmount : MonoBehaviour
    {
        ScrollRect scroll;

        void OnEnable()
        {
            if (!scroll)
            {
                scroll = GetComponent<ScrollRect>();
            }

            scroll.verticalNormalizedPosition = 1;
        }
    }

}