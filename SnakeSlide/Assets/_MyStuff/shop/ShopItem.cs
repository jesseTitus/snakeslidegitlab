using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace SnakeSlide
{
    public abstract class ShopItem : IntEventInvoker
    {
        #region fields
        protected Image shopItemBG;
        protected TextMeshProUGUI costText;   // cost
        protected TextMeshProUGUI titleText;
        protected Image itemIconImageComponent;           // what am I buying?
        protected Image shopItemHighlight;
        protected GameObject gemIcon;          // gem icon (shown if not owned)
        protected UIManager uiManager;
        protected bool initialized;

        #endregion

        #region abstract Properties
        protected abstract Color BgColor { get; }
        protected abstract Sprite ItemBg { get; }
        protected abstract int ItemCost { get ; }
        protected abstract int ItemNumber { get; }
        protected abstract bool IsItemOwned { get; }
        protected abstract bool IsItemEquipped { get; }
        #endregion

        #region Properties
        protected virtual bool HasIcon { get { return false; } }
        protected virtual Sprite Icon { get; }
        protected bool PurchaseSuccessful
        {
            get { return GM._instance.SpendIfCanAfford(ItemCost); }
        }
        protected virtual bool IsConfirmationButton { get { return false; } }
        protected virtual string TitleText { get { return ""; } }
        //protected virtual Image.Type IconImageType { get { return Image.Type.Simple; } }
        #endregion

        public virtual void Start()
        {
            #region get components
            costText = GetComponentInChildren<ShopItemCostComponent>().GetComponent<TextMeshProUGUI>();
            titleText = GetComponentInChildren<ShopTitleComponent>().GetComponent<TextMeshProUGUI>();
            itemIconImageComponent = GetComponentInChildren<ShopItemIconComponent>().GetComponent<Image>();
            shopItemHighlight = GetComponentInChildren<ShopHighlightComponent>().Image;
            shopItemBG = GetComponentInChildren<ShopItemBGComponent>().GetComponent<Image>();
            gemIcon = GetComponentInChildren<ShopItemGemIcon>().gameObject;
            uiManager = FindObjectOfType<UIManager>();
            #endregion

            //Events
            unityEvents[EventName.FailedPurchaseInadequateGems] = new ShopPurchaseFailedEvent();
            EventManager.AddInvoker(EventName.FailedPurchaseInadequateGems, this);
            unityEvents[EventName.OpenShopPurchaseConfirmation] = new OpenShopPurchaseConfirmEvent();
            EventManager.AddInvoker(EventName.OpenShopPurchaseConfirmation, this);


            Initialize();
        }

        void Initialize()
        {
            itemIconImageComponent.enabled = HasIcon;
            if (HasIcon)
            {
                //itemIconImageComponent.type = IconImageType;    //TODO - can remove?
                itemIconImageComponent.sprite = Icon;
            }
            shopItemBG.sprite = ItemBg;
            titleText.text = TitleText;
            shopItemBG.color = BgColor;
        }

        #region Methods


        public void SelectShopItem()
        {
            if (IsItemEquipped)
            {
                return;
            }

            

            // Process item to either attempt purchase or equip if owned
            if (!IsItemEquipped)
            {
                AudioManager.PlaySound(SoundEffect.ShopSelect);

            }
            if (!IsItemOwned)
            {
                if (!IsConfirmationButton)
                {
                    // Just clicked on a shop item that isn't owned 
                    // - open confirm panel
                    // - send item info
                    // TODO - move to SendConfirmationData()
                    //Debug.Log("Just clicked shop item (not owned)... open confirm panel");
                    DebugLogger._instance.Log("Just clicked shop item (not owned)... open confirm panel");
                    SendConfirmationData();
                    return;
                }
                if (!PurchaseSuccessful)
                {
                    unityEvents[EventName.FailedPurchaseInadequateGems].Invoke(-1);
                    return;
                }

                // purchase success - invoke purchase event
                try
                {
                    AttemptPurchase();
                    AudioManager.PlaySound(SoundEffect.ShopPurchase);
                }
                catch
                {
                    //Debug.Log("Item purchase failed");
                    DebugLogger._instance.Log("Item purchase failed");
                }
            }

            try
            {
                EquipSelectedItem();
            }
            catch
            {
                //Debug.Log("Equip item failed!");
                DebugLogger._instance.Log("Equip item failed!");
            }

        }


        public virtual void SendConfirmationData()
        {
            unityEvents[EventName.OpenShopPurchaseConfirmation].Invoke((int)ItemNumber);
        }

        protected virtual void UpdateVisuals()
        {
            itemIconImageComponent.type = Image.Type.Simple;
            shopItemHighlight.enabled = !IsItemEquipped;
            if (IsItemOwned)
            {
                // Item is owned - display status in shop
                costText.text = IsItemEquipped ? "Equipped" : "";
                gemIcon.SetActive(false);
            }
            else
            {
                costText.text = ItemCost.ToString();
                gemIcon.SetActive(true);
            }
        }

        protected abstract void AttemptPurchase();

        protected abstract void EquipSelectedItem();

        void OnEnable()
        {
            if (initialized)
            {
                UpdateVisuals();
            }
        }



        #endregion









    }
}