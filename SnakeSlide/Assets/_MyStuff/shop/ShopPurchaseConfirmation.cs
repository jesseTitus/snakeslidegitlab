using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SnakeSlide
{
    public class ShopPurchaseConfirmation : MonoBehaviour
    {
        [SerializeField]
        GameObject fadePanel = null;

        BigSkinButton _skinButton;
        BigBackgroundButton _bgButton;
        BigSoundtrackButton _soundtrackButton;


        ShopItem selectionPlaceholder;
        Button buyButton;

        void Start()
        {
            fadePanel.SetActive(false);
            EventManager.AddListener(EventName.OpenShopPurchaseConfirmation, OnOpenConfirmation);
        }

        void OnOpenConfirmation(int item)
        {
            fadePanel.SetActive(true);
            if (!buyButton)
            {
                buyButton = GetComponentInChildren<BuyButtonComponent>().GetComponent<Button>();
            }


            if (item < (int)Item.Bg1)
            {
                _skinButton = gameObject.AddComponent<BigSkinButton>();
                _skinButton.mySkin = (Skin)item;
                selectionPlaceholder = _skinButton;
                //buyButton.onClick.AddListener(_skinButton.SelectShopItem);
            } else if (item < (int)Item.Soundtrack1)
            {
                _bgButton = gameObject.AddComponent<BigBackgroundButton>();
                _bgButton.myTileType = (TileType)item - (int)Item.Bg1;
                selectionPlaceholder = _bgButton;

                //buyButton.onClick.AddListener(_bgButton.SelectShopItem);
            } else
            {
                _soundtrackButton = gameObject.AddComponent<BigSoundtrackButton>();
                _soundtrackButton.mySoundtrack = (SoundTrack)item - (int)Item.Soundtrack1;
                selectionPlaceholder = _soundtrackButton;

                //buyButton.onClick.AddListener(_soundtrackButton.SelectShopItem);
            }

            // Assign Buy Button to newly added component (skin,bg, soundtrack)
            buyButton.onClick.AddListener(selectionPlaceholder.SelectShopItem);
            buyButton.onClick.AddListener(OnClickCancel);
        }

        #region Button Actions
        public void OnClickCancel()
        {
            fadePanel.SetActive(false);

            // Remove component
            if (_skinButton)
            {
                Destroy(_skinButton);

            } else if (_bgButton)
            {
                Destroy(_bgButton);

            } else if (_soundtrackButton)
            {
                Destroy(_soundtrackButton);
            }
        }


        #endregion
    }
}
