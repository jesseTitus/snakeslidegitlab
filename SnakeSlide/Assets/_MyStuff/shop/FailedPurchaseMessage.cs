using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class FailedPurchaseMessage : MonoBehaviour
    {
        Animator anim;

        void Start()
        {
            anim = GetComponent<Animator>();
            EventManager.AddListener(EventName.FailedPurchaseInadequateGems, OnPurchaseFailed);
        }

        void OnPurchaseFailed(int unused)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            {
                anim.SetTrigger("ShopPurchaseFailedTrigger");
            }
        }
    } 
}
