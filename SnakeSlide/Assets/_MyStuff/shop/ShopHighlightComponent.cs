using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SnakeSlide
{
    public class ShopHighlightComponent : MonoBehaviour
    {
        [SerializeField]
        Image image = null;

        public Image Image { get { return image; } }
    }

}