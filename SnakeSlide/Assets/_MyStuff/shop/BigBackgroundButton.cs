using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class BigBackgroundButton : BackgroundButton
    {
        protected override bool IsConfirmationButton
        {
            get { return true; }
        }

        protected override void UpdateVisuals()
        {
            //itemIconImageComponent.type = UnityEngine.UI.Image.Type.Tiled;
            costText.text = ItemCost.ToString();
        }
    } 
}
