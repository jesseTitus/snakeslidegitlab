﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace SnakeSlide
{
    public class Point : Destroyable
    {
        const string TutorialMsg = "Grab to grow!";
        
        CircleCollider2D coll;
        SpriteRenderer spriteRenderer;


        void Start()
        {
            unityEvents[EventName.PointCollected] = new PointCollectedEvent();
            EventManager.AddInvoker(EventName.PointCollected, this);
            coll = GetComponent<CircleCollider2D>();
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected override void OnDestroyed(Collider2D other)       // called by OnTriggerEnter2D(other)
        {
            coll.enabled = false;
            spriteRenderer.enabled = false;

            AudioManager.PlaySound(SoundEffect.Point);
            unityEvents[EventName.PointCollected].Invoke(1);
        }

        void OnEnable()
        {
            GetComponent<CircleCollider2D>().enabled = true;
            GetComponent<SpriteRenderer>().enabled = true;
        }

        #region tutorial stuff
        public void AddTutorialMessage()
        {
            //Debug.Log("Adding tutorial message to this point");
            var t = Instantiate(DataBase.PlayerTutCanvas, transform);
            t.GetComponentInChildren<TextMeshProUGUI>().text = TutorialMsg;
        }
        #endregion
    }
}
