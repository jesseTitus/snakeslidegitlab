using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace SnakeSlide
{
    public class AddShadowToTilemap : MonoBehaviour
    {
        //Vector2 offset = new Vector2(-0.1f, -0.5f);

        TilemapRenderer casterRenderer;
        TilemapRenderer shadowRenderer;

        Transform transCaster;

        Material shadowMaterial;

        void Start()
        {
            shadowMaterial = DataBase.ShadowMaterial;
            transCaster = transform;

            var shadow = CreateShadow(30/250f, Color.black, new Vector2(-0.1f, -0.5f), "Shadow", gameObject);
            CreateShadow(1, Color.black, new Vector2(-0.025f, -0.0125f), "Trim", shadow);
        }

        GameObject CreateShadow(float shadowAlpha, Color shadowColor, Vector2 offset, string name, GameObject copy)
        {
            var obj = copy;
            
            Transform shadow = Instantiate(obj.transform, transform);
            shadow.gameObject.name = name;
            Destroy(shadow.GetComponent<Wall>());
            Destroy(shadow.GetComponent<AddShadowToTilemap>());
            Destroy(shadow.GetComponent<CompositeCollider2D>());
            Destroy(shadow.GetComponent<Rigidbody2D>());
            Destroy(shadow.GetComponent<TilemapCollider2D>());

            casterRenderer = transCaster.gameObject.GetComponent<TilemapRenderer>();
            shadowRenderer = shadow.gameObject.GetComponent<TilemapRenderer>();

            shadowRenderer.material = shadowMaterial;
            var temp = shadowColor;
            temp.a = shadowAlpha;
            shadow.GetComponent<Tilemap>().color = temp;
            shadowRenderer.sortingLayerName = casterRenderer.sortingLayerName;
            shadowRenderer.sortingOrder = casterRenderer.sortingOrder - 1;


            shadow.position = new Vector2(transCaster.position.x + offset.x,
                transCaster.position.y + offset.y);

            return shadow.gameObject;
        }
    }
}