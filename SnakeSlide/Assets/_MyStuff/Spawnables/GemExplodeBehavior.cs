using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemExplodeBehavior : MonoBehaviour
{
    [Header("References")]
    public GameObject gemMesh;           
    public GameObject collectedParticleSystem;
    
    CircleCollider2D circleCollider2D;

    float durationOfCollectedParticleSystem;

    void Start()
    {
        durationOfCollectedParticleSystem = collectedParticleSystem.GetComponent<ParticleSystem>().main.duration;
        circleCollider2D = GetComponent<CircleCollider2D>();
    }

    public void Explode()
    {
        gemMesh.SetActive(false);
        collectedParticleSystem.SetActive(true);
        circleCollider2D.enabled = false;
        //Invoke("DeactivateGemGameObject", durationOfCollectedParticleSystem);
    }

    //void DeactivateGemGameObject()
    //{
    //    circleCollider2D.enabled = false;
    //}

    void OnEnable()
    {
        GetComponent<CircleCollider2D>().enabled = true;
        gemMesh.SetActive(true);
    }
}
