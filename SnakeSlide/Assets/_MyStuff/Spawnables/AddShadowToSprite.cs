using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    [DisallowMultipleComponent]
    public class AddShadowToSprite : MonoBehaviour
    {

        SpriteRenderer casterRenderer;
        SpriteRenderer shadowRenderer;

        Transform transCaster;
        

        void Start()
        {
            transCaster = transform;


            CreateShadow(30 / 250f, Color.black, new Vector2(-0.1f, -0.5f), "Shadow");
            CreateShadow(1f, Color.black, new Vector2(-0.025f, -0.025f), "Trim");
        }

        void CreateShadow(float shadowAlpha, Color shadowColor, Vector2 offset, string name)
        {
            Transform shadow = new GameObject().transform;
            shadow.parent = transCaster;
            shadow.gameObject.name = name;
            shadow.localRotation = Quaternion.identity;
            shadow.localScale = new Vector2(1f, 1f);

            casterRenderer = GetComponent<SpriteRenderer>();
            shadowRenderer = shadow.gameObject.AddComponent<SpriteRenderer>();

            shadowRenderer.material = DataBase.ShadowMaterial;
            var temp = shadowColor;
            temp.a = shadowAlpha;
            shadowRenderer.color = temp;
            shadowRenderer.sortingLayerName = casterRenderer.sortingLayerName;
            shadowRenderer.sortingOrder = casterRenderer.sortingOrder - 1;


            //transShadow.position = new Vector2(transCaster.position.x + offset.x,
            //    transCaster.position.y + offset.y);
            shadow.position = new Vector2(transCaster.position.x + offset.x,
                transCaster.position.y + offset.y);
            shadowRenderer.sprite = casterRenderer.sprite;
        }

        //void LateUpdate()
        //{
        //    //TODO - do i need this here?
        //    transShadow.position = new Vector2(transCaster.position.x + offset.x,
        //        transCaster.position.y + offset.y);
        //    shadowRenderer.sprite = casterRenderer.sprite;
        //}
    }

}