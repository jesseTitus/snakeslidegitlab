﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public abstract class Destroyable : IntEventInvoker
    {
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponentInParent<Player>())
            {
                OnDestroyed(other);
            }

        }

        protected abstract void OnDestroyed(Collider2D other);
    }

}