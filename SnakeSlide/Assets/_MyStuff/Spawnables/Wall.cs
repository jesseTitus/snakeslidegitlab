﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Linq;
using System;

namespace SnakeSlide
{
    public class Wall : IntEventInvoker
    {
        const string SortLayerName = "Walls";

        #region fields
        Difficulty WallDifficulty;  // to identity when returning to obj pool
        #endregion

        #region properties
        WallShape Shape { get { return DataBase.wallShapesDict.FirstOrDefault(
                x => x.Value.ShapeSprite.Equals(spriteRenderer.sprite)).Key.Item1; } }
        #endregion

        #region components
        SpriteRenderer spriteRenderer;
        GameObject shadow;
        Tilemap tilemap;
        #endregion

        void Awake()
        {
            //DebugLogger._instance.Log("Awake");

            spriteRenderer = GetComponent<SpriteRenderer>();
            var tilemapRenderer = GetComponent<TilemapRenderer>();
            if (spriteRenderer != null)
            {
                spriteRenderer.sortingLayerName = SortLayerName;
                if (!GetComponent<AddShadowToSprite>())
                {
                    gameObject.AddComponent<AddShadowToSprite>();
                }
            }
            else if (tilemapRenderer != null)
            {
                tilemapRenderer.sortingLayerName = SortLayerName;
                if (!GetComponent<AddShadowToTilemap>())
                {
                    gameObject.AddComponent<AddShadowToTilemap>();
                }
            }


            tilemap = GetComponent<Tilemap>();


        }
        void Start()
        {
            // Invoke
            unityEvents[EventName.WallHit] = new WallHitEvent();
            EventManager.AddInvoker(EventName.WallHit, this);
            unityEvents[EventName.PlayerDied] = new PlayerDied();
            EventManager.AddInvoker(EventName.PlayerDied, this);

            // Listen
            EventManager.AddListener(EventName.BackgroundEquipped, OnBackgroundEquipped);


            UpdateVisuals();
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponentInParent<Player>() == null)
            {
                return;
            }


            unityEvents[EventName.WallHit].Invoke(-1);
            DebugLogger._instance.Log("Died by hitting: " + transform.parent.name);
        }

        void UpdateVisuals()
        {
            if (DataBase.tileDict.Count <= 0)
            {
                return;
            }

            if (!tilemap)
            {
                // Wall is built with sprites (circle, triangle...) swap sprites and add correct collider
                UpdateSpriteAndCollider();

            } else if (DataBase.tileDict[TileType.Grass].TileAsset !=
                        DataBase.tileDict[GM._instance.BackgroundEquipped].TileAsset)
            {
                // Wall is built from tilemap, swap tiles
                SwapTiles();
            }
            
        }

        void SwapTiles()
        {
            tilemap.SwapTile(DataBase.tileDict[TileType.Grass].TileAsset,
                    DataBase.tileDict[GM._instance.BackgroundEquipped].TileAsset);
        }

        void UpdateSpriteAndCollider()
        {
            // Get shape that matches current one
            WallShape shape = DataBase.wallShapesDict.FirstOrDefault(
                x => x.Value.ShapeSprite.Equals(spriteRenderer.sprite)).Key.Item1;

            //Debug.Log("Shape: " + shape);
            //Debug.Log("BG: " + GM._instance.BackgroundEquipped);

            var tuple = new Tuple<WallShape, TileType>(
                Shape, GM._instance.BackgroundEquipped);

            // Update sprite to match current shape AND bgEquipped
            spriteRenderer.sprite = DataBase.wallShapesDict[tuple].ShapeSprite;
            //Debug.Log("Turn into: " + DataBase.wallShapesDict[tuple].ShapeSprite);
            //Debug.Log("Dark blue right tri: " + DataBase.wallShapesDict[new Tuple<WallShape, TileType>(WallShape.RightTriangle, TileType.WaterDark)]);

            // Add suitable collider (circle for round)
            if (Shape == WallShape.Round)
            {
                Destroy(GetComponent<PolygonCollider2D>());
                var collider = gameObject.AddComponent<CircleCollider2D>();
                collider.isTrigger = true;
            } else if (Shape == WallShape.Oval)
            {
                Destroy(GetComponent<PolygonCollider2D>());
                var collider = gameObject.AddComponent<CapsuleCollider2D>();
                collider.isTrigger = true;
            }
        }


        void OnBackgroundEquipped(int unused)
        {
            UpdateVisuals();
        }
        
    }
}