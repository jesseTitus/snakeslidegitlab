﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class ObjectDestroyer : MonoBehaviour
    {

        /// <summary>
        /// When an object enters the bottom boundary of the screen
        /// and it is of type Destroyable
        /// it will be destroyed
        /// </summary>
        /// <param name="other"></param>
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<Destroyable>())
            {
                //var objParent = other.gameObject.transform.parent.gameObject;
                //DebugLogger._instance.Log("Deactivated: " + objParent.name);
                Destroy(other.gameObject);
                //objParent.SetActive(false);
            }
        }
    }

}