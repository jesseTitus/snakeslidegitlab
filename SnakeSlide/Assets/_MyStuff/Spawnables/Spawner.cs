﻿//using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
//using UnityEditor.Presets;
using UnityEngine;
//using Random = System.Random;

namespace SnakeSlide
{
    /// <summary>
    /// The Spawner's job is to keep a list of
    /// wall objects (WallLeft, WallRight, Center)
    /// and spawn them as needed
    /// </summary>
    public class Spawner : MonoBehaviour
    {
        GameObject IsometricGrid;
        
        public static Spawner _instance;

        [SerializeField]
        GameObject StarterWall = null; //keep track of so can deactivate when -> outdatedLevel

        [SerializeField]
        GameObject[] WallPrefabsEasy = null;  
        [SerializeField]
        GameObject[] WallPrefabsNormal = null;
        [SerializeField]
        GameObject[] WallPrefabsHard = null;
        static Dictionary<Difficulty, GameObject[]> levelPrefabs = new Dictionary<Difficulty, GameObject[]>();

        static Dictionary<Difficulty, List<GameObject>> levels = new Dictionary<Difficulty, List<GameObject>>();

        // Track recent level segments
        [SerializeField]
        GameObject currentLevel;
        [SerializeField]
        GameObject prevLevel;
        [SerializeField]
        GameObject outdatedLevel;

        void Awake()
        {
            #region singleton
            if (_instance == null)
            {
                _instance = this;
            }
            else if (_instance != this)
            {
                Destroy(this);
            }
            #endregion

            Random.InitState(System.DateTime.Now.Millisecond);
            //Random.seed = System.DateTime.Now.Millisecond;

            levelPrefabs[Difficulty.Easy] = WallPrefabsEasy;
            levelPrefabs[Difficulty.Normal] = WallPrefabsNormal;
            levelPrefabs[Difficulty.Hard] = WallPrefabsHard;
        }

        void Start()
        {
            IsometricGrid = GameObject.FindGameObjectWithTag("IsometricGrid");

            currentLevel = StarterWall;

            // create object-pool for levels instead of using Instantiate/Destroy all the time
            foreach (var diff in System.Enum.GetValues(typeof(Difficulty)))
            {
                levels[(Difficulty)diff] = new List<GameObject>();
                foreach (var levelPrefab in levelPrefabs[(Difficulty)diff])
                {
                    var level = Instantiate(levelPrefab, IsometricGrid.transform);
                    level.SetActive(true);
                    level.SetActive(false);
                    levels[(Difficulty)diff].Add(level);
                }
            }
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                // Procedural generation
                // This section decides what pieces to spawn

                var difficulty = (Difficulty)((int)(transform.position.y / ConfigUtils.SpawnerDifficultyInterval) % 3);   // integer division
                //Debug.Log("Difficulty: " + difficulty);

                var possibleLevelsAtThisDifficulty = levelPrefabs[difficulty];
                var num = Random.Range(0, possibleLevelsAtThisDifficulty.Length);
                var spawnedPiece = possibleLevelsAtThisDifficulty[num];
                //Debug.Log("possible levels at this difficulty: " + possibleLevelsAtThisDifficulty.Length);
                //Debug.Log("number chose: " + num);

                /*for(var i=0; i< 999;i++)  // test randomness
                {
                    Debug.Log(Random.Range(0, possibleLevelsAtThisDifficulty.Length));
                }*/


                var spawnVector = new Vector3(spawnedPiece.transform.position.x,
                    transform.position.y + spawnedPiece.transform.position.y);
                //var newPiece = Instantiate(spawnedPiece, IsometricGrid.transform);
                //newPiece.transform.position = spawnVector;

                // track prev piece so we can deactivate it
                if (prevLevel != null)
                {
                    prevLevel.SetActive(false);
                }
                outdatedLevel = prevLevel;
                prevLevel = currentLevel;


                currentLevel = levels[difficulty][num];
                currentLevel.SetActive(true);
                currentLevel.transform.position = spawnVector;

                MoveSpawnerToNextArea();
            }
        }

        void MoveSpawnerToNextArea()
        {
            transform.position += new Vector3(0, ConfigUtils.SpawnerJumpRate, 0);
        }
    }

}