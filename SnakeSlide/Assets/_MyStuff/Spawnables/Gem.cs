﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace SnakeSlide
{
    public class Gem : Destroyable
    {
        public Transform gemMesh;
        const string TutorialMsg = "Grab For Cash!";

        void Start()
        {
            unityEvents[EventName.GemCollected] = new GemCollectedEvent();
            EventManager.AddInvoker(EventName.GemCollected, this);
        }

        protected override void OnDestroyed(Collider2D other)
        {
            unityEvents[EventName.GemCollected].Invoke(1);
            GetComponent<GemExplodeBehavior>().Explode();
        }
        

        #region tutorial stuff
        public void AddTutorialMessage()
        {
            var t = Instantiate(DataBase.PlayerTutCanvas, gemMesh);
            t.GetComponentInChildren<TextMeshProUGUI>().text = TutorialMsg;
        }
        #endregion
    }
}
