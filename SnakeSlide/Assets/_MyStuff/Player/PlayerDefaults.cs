using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerDefaults : ScriptableObject
{
    public WaitForSeconds PlayerDeathAnimationWait { get { return new WaitForSeconds(PlayerDeathAnimationLength); } }
    public WaitForSeconds PlayerBoostDurationWait { get { return new WaitForSeconds(PlayerBoostDuration); } }

    [Header("Head")]
    public float PlayerBoostSpeed;
    public float PlayerBoostDuration;
    public float PlayerMaxSpeed;
    public float PlayerAcceleration;
    public float PlayerMinRotateSpeed;
    public float PlayerMaxRotateSpeed;
    public float PlayerRotateAcceleration;
    public float PlayerDeathAnimationLength;
    public float PlayerMaxBodyParts;


    [Header("Body")]
    public int NodesOfSeparation;

    public PlayerDefaults()
    {

    }
}
