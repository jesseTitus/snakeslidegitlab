using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class Head : IntEventInvoker
    {
        void Start()
        {
            // Invoke
            unityTEvents[EventName.SendCoords] = new SendCoordsEvent();
            EventManager.AddTInvoker(EventName.SendCoords, this);

            // Listen
            EventManager.AddListener(EventName.Play, OnGameStart);
        }

        void Update()
        {
            unityTEvents[EventName.SendCoords].Invoke(transform);
        }

        void OnGameStart(int unused)
        {
            GetComponent<SpriteRenderer>().sprite = SkinDatabase.skinDictionary[GM._instance.SkinEquipped].Head;
        }
    }

}