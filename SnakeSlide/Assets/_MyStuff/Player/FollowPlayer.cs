﻿using System.Collections;
using System.Collections.Generic;
using System.Security;
using UnityEngine;

namespace SnakeSlide
{
    public class FollowPlayer : MonoBehaviour
    {
        [SerializeField]
        bool followHor = false;
        [SerializeField]
        bool followVert = false;
        [SerializeField]
        bool followZ = false;

        Vector3 vec;
        float yDistance = 0;


        void Start()
        {
            EventManager.AddTListener(EventName.SendCoords, OnReceivedCoordsEvent);

            vec = transform.position;
            yDistance = transform.position.y;
        }
        
        // Follow object when coords received
        void OnReceivedCoordsEvent(Transform t)
        {
            if (followHor)
            {
                vec = new Vector3(t.position.x, vec.y, vec.z);
            }

            if (followVert)
            {
                vec = new Vector3(vec.x, t.position.y + yDistance, vec.z);
            }

            if (followZ)
            {
                vec = new Vector3(vec.x, vec.y, t.position.z);
            }

            transform.position = vec;
        }
    }

}