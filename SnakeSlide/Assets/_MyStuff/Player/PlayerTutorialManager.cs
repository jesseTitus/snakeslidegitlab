using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace SnakeSlide
{
    /// <summary>
    /// Add a tutorial canvas prefab as child
    /// OnClickScreen self destruct
    /// </summary>
    public class PlayerTutorialManager : IntEventInvoker
    {
        GameObject tutorialObj;
        TextMeshProUGUI tutorialText;
        bool pointCollected;
        bool gemCollected;

        enum TutorialState
        {
            TapToStart,
            HoldToRotate,
            WaitForCollect,
            Finish
        }

        TutorialState state;
        TutorialState State
        {
            set
            {
                state = value;

                switch (state)
                {
                    case TutorialState.TapToStart:
                        Time.timeScale = 0f;
                        tutorialText.text = "Tap to start";
                        break;

                    case TutorialState.HoldToRotate:
                        Time.timeScale = 1f;
                        FindPointAndGem();
                        tutorialText.text = "Hold to rotate";
                        unityEvents[EventName.ShowTutorialB].Invoke(-1);
                        break;

                    case TutorialState.WaitForCollect:
                        tutorialText.text = "";
                        break;

                    case TutorialState.Finish:
                        //Debug.Log("Tutorial complete");
                        DebugLogger._instance.Log("Tutorial complete");
                        PlayerPrefs.SetInt("ShowTutorial", 1);
                        PlayerPrefs.Save();
                        unityEvents[EventName.EndTutorial].Invoke(-1);
                        SelfDestruct();
                        break;
                }
            }
        }

        void Start()
        {
            Initialize();
        }

        void Initialize()
        {
            tutorialObj = Instantiate(DataBase.PlayerTutCanvas, transform.GetChild(0));
            tutorialText = tutorialObj.transform.GetComponentInChildren<TextMeshProUGUI>();
            
            // Listen
            EventManager.AddListener(EventName.PointCollected, OnPointCollected);
            EventManager.AddListener(EventName.GemCollected, OnGemCollected);
            
            // Invoke
            unityEvents[EventName.ShowTutorialB] = new ShowTutorial();
            EventManager.AddInvoker(EventName.ShowTutorialB, this);
            unityEvents[EventName.EndTutorial] = new ShowTutorial();
            EventManager.AddInvoker(EventName.EndTutorial, this);

            State = TutorialState.TapToStart;
        }

        void Update()
        {
            var press = Input.GetButtonDown("Fire1");
            var release = Input.GetButtonUp("Fire1");
            if (press && state == TutorialState.HoldToRotate || 
                release && state == TutorialState.TapToStart)
            {
                NextState();
            }
        }

        void FindPointAndGem()
        {
            var points = FindObjectsOfType<Point>().
                                                    OrderBy(p => p.transform.position.y);
            var firstPoint = points.Min();
            var gems = FindObjectsOfType<Gem>().
                OrderBy(g => g.transform.position.y);
                //.Single()
            var firstGem = gems.Min();
            if (firstPoint)
            firstPoint.AddTutorialMessage();
            if (firstGem)
            firstGem.AddTutorialMessage();
        }

        void NextState()
        {
            if (state == TutorialState.TapToStart)
            {
                State = TutorialState.HoldToRotate;

            }
            else if (state == TutorialState.HoldToRotate)
            {
                State = TutorialState.WaitForCollect;

            } else if (state == TutorialState.WaitForCollect)
            {
                State = TutorialState.Finish;
            }
        }

        void SelfDestruct()
        {
            Destroy(this);
            Destroy(tutorialObj);
            
            EventManager.RemoveInvoker(EventName.ShowTutorialB, this);
            EventManager.RemoveInvoker(EventName.EndTutorial, this);
        }
        
        //void OnTapToStart(int unused)
        //{
        //    State = TutorialState.TapToStart;
        //}

        void OnPointCollected(int unused)
        {
            pointCollected = true;
            if (gemCollected)
                NextState();
        }

        void OnGemCollected(int unused)
        {
            gemCollected = true;
            if (pointCollected)
                NextState();
        }
    } 
}
