﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace SnakeSlide
{
    public class Player : IntEventInvoker
    {
        #region constants
        const float maxAngleR = -45f;
        const float maxAngleL = 45f;
        #endregion
        

        #region fields
        [SerializeField]
        bool gemGivesBoost = false;     // whether grabbing a gem creates a boost in speed
        [SerializeField]
        int startingSize = 0;      // for debugging only (start with long snake)
        [SerializeField]
        float curSpd = 0f;   // this speed is what actually controls the player, it slowly increases * acceleration
        [SerializeField]
        float rotateSpeed;
        float direction;
        bool isPlayerDead;

        #endregion
        
        
        public List<Transform> BodyParts = new List<Transform>();
        public Transform Tail { get { return BodyParts[BodyParts.Count - 1]; } }
        public static Player _instance;
        [SerializeField]
        GameObject bodyPrefab = null;
        [SerializeField]
        PlayerDefaults playerDefaults = null;

        #region components
        TransformationBuffer _transformationBuffer;
        #endregion

        void Start()
        {
            #region singleton
            if (_instance == null)
            {
                _instance = this;
            }
            else if (_instance != this)
            {
                Destroy(this);
            }
            #endregion

            #region tutorial
            if (PlayerPrefs.GetInt("ShowTutorial") == 0)
            {
                gameObject.AddComponent<PlayerTutorialManager>();
            } else
            {
                foreach(var t in FindObjectsOfType<TutorialItem>())
                {
                    Destroy(t.gameObject);
                }
            }
            #endregion

            #region events
            // Listen
            EventManager.AddListener(EventName.WallHit, OnWallHitEvent);
            EventManager.AddListener(EventName.PointCollected, OnPointCollectedEvent);
            EventManager.AddListener(EventName.GemCollected, OnGemCollectedEvent);
            EventManager.AddListener(EventName.Play, OnGameStart);

            // Invoke
            unityEvents[EventName.SendPlayerCurSpd] = new SendCurrentPlayerSpeedEvent();
            EventManager.AddInvoker(EventName.SendPlayerCurSpd, this);
            unityEvents[EventName.PlayerDied] = new PlayerDied();
            EventManager.AddInvoker(EventName.PlayerDied, this);
            unityEvents[EventName.PointCollected] = new PointCollectedEvent();
            EventManager.AddInvoker(EventName.PointCollected, this);
            #endregion

            #region components
            _transformationBuffer = GetComponent<TransformationBuffer>();
            #endregion
        }

        void OnGameStart(int unused)
        {
            for (var i = 0; i < startingSize - 1; i++)
            {
                AddBodyPart();
            }

            StartCoroutine("CheckLeftScreen");
            StartCoroutine("RevEngine");
        }

        void Update()
        {
            Move();

            unityEvents[EventName.SendPlayerCurSpd].Invoke((int)(curSpd * 100));

#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(1))
            {
                unityEvents[EventName.PointCollected].Invoke(1);
            }
#endif

            // Make bodyparts follow head-trail
            for (var i= 1; i < BodyParts.Count; i++)
            {
                var bodyPart = BodyParts[i];
                var extraNodesForTail = bodyPart == Tail ? 1 : 0;   //give tail extra nodes for overlap
                var moveTo = _transformationBuffer.Transformations[_transformationBuffer.Length - i * playerDefaults.NodesOfSeparation - 1 /*- extraNodesForTail*/];
                
                bodyPart.position = moveTo.position;
                bodyPart.rotation = moveTo.rotation;
            }
        }


        IEnumerator CheckLeftScreen()
        {
            float leftX = Camera.main.ViewportToWorldPoint(Vector3.zero).x;                 // TODO - replace with ScreenUtils
            float rightX = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0)).x;

            while (!isPlayerDead)
            {
                if (BodyParts[0].position.x <= leftX || BodyParts[0].position.x >= rightX)
                {
                    isPlayerDead = true;
                    break;
                }
                yield return null;
            }
            AudioManager.PlaySound(SoundEffect.Die);
            curSpd = 0f;
            foreach (var b in BodyParts)
            {
                b.GetComponent<SpriteRenderer>().enabled = false;
            }
            GetComponent<ExplodeBehavior>().Explode();
            yield return playerDefaults.PlayerDeathAnimationWait;
            Destroy(gameObject);
            unityEvents[EventName.PlayerDied].Invoke(-1);
            StopCoroutine("CheckLeftScreen");
            yield return null;
        }

        
        public void Move()
        {
            #region choose direction based on input
            //snake head always rotated right(-45), unless player input(+45)
            if (Input.GetMouseButton(0))
            {
                if (direction == maxAngleR)
                {
                    direction = maxAngleL;
                }
            }
            else if (direction == maxAngleL)
            {
                direction = maxAngleR;
            }
            else
            {
                direction = maxAngleR;
            }
            #endregion
            
            rotateSpeed = playerDefaults.PlayerMaxRotateSpeed;

            var newR = Quaternion.AngleAxis(direction, Vector3.forward);
            BodyParts[0].rotation = Quaternion.Slerp(BodyParts[0].transform.rotation,
                                                        newR,
                                                        rotateSpeed * Time.deltaTime);


            BodyParts[0].position += BodyParts[0].up * curSpd * Time.deltaTime;
        }

        public void AddBodyPart()
        {
            #region exit statement
            if (BodyParts.Count > playerDefaults.PlayerMaxBodyParts)           
            {
                return;
            }
            #endregion

            // create bodypart
            Transform newPart = (Instantiate(bodyPrefab, BodyParts[BodyParts.Count - 1].position,
                                                BodyParts[BodyParts.Count - 1].rotation)
                                                as GameObject).transform;

            //var bodyRenderer = newPart.GetComponent<SpriteRenderer>();
            //bodyRenderer.sprite = SkinDatabase.
            //                    skinDictionary[GM._instance.SkinEquipped].Body;

            newPart.SetParent(transform);

            // Add new bodypart to Player.cs list
            BodyParts.Add(newPart);

            // update to be body or tail (skip head and 1st segment, i.e. start at 1)
            //for (var i=2; i<BodyParts.Count; i++)
            //{
            //    var bodyPart = BodyParts[i];
            //    bodyPart.GetComponent<BodyPart>().UpdateSprite(i == BodyParts.Count - 1);
            //}
        }

        IEnumerator RevEngine()
        {
            while (curSpd < playerDefaults.PlayerMaxSpeed)
            {
                curSpd += playerDefaults.PlayerAcceleration * Time.deltaTime;
                yield return null;
            }

            curSpd = playerDefaults.PlayerMaxSpeed;
            //Debug.Log("Player speed reached peak... ending coroutine RevEngine");
            StopCoroutine("RevEngine");
        }

        public void Boost()
        {
            if (gemGivesBoost)
            {
                StartCoroutine("BoostCo");
            }
        }
        IEnumerator BoostCo()
        {
            curSpd *= playerDefaults.PlayerBoostSpeed;
            yield return playerDefaults.PlayerBoostDurationWait;
            curSpd = playerDefaults.PlayerMaxSpeed;
        }

        

        #region EVENTS

        void OnWallHitEvent(int unused)
        {
            isPlayerDead = true;
        }

        void OnPointCollectedEvent(int unused)
        {
            AddBodyPart();
        }

        void OnGemCollectedEvent(int unused)
        {
            Boost();
        }
        #endregion
    }
}