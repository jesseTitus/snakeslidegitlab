﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public enum Skin
    {
        Original,
        Maple,
        TreeViper,
        Cobra,
        Rainbow,
        BlackMamba
    }
}