﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class BodyPart : MonoBehaviour
    {
        SpriteRenderer _renderer;

        void Awake()
        {
            _renderer = GetComponent<SpriteRenderer>();
            _renderer.sprite = SkinDatabase.skinDictionary[GM._instance.SkinEquipped].Body;
        }
        void Start()
        {
            _renderer.sprite = SkinDatabase.skinDictionary[GM._instance.SkinEquipped].Body;
        }
        

        public void UpdateSprite(bool isTail)
        {
            //if (/*Player._instance.Tail*/ Player._instance.BodyParts[Player._instance.BodyParts.Count-1] == transform)
            //{
            //    Debug.Log("I am the last body part - change to tail!");
            //    _renderer.sprite = SkinDatabase.skinDictionary[GM._instance.SkinEquipped].Tail;
            //} else
            //{
            //    _renderer.sprite = SkinDatabase.skinDictionary[GM._instance.SkinEquipped].Body;
            //}
            if (isTail)
            {
                _renderer.sprite = SkinDatabase.skinDictionary[GM._instance.SkinEquipped].Tail;
            } else
            {
                _renderer.sprite = SkinDatabase.skinDictionary[GM._instance.SkinEquipped].Body;
            }
        }
    }

}