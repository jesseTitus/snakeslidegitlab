using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundTrack
{
    Soundtrack1,
    Soundtrack2,
    Soundtrack3,
    Soundtrack4,
    Soundtrack5,
    Soundtrack6
}
