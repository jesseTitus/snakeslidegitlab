using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace SnakeSlide
{
    public class GameAudioSource : MonoBehaviour
    {

        void Awake()
        {
            if (!AudioManager.Initialized)
            {
                //initialize audio manager and persist audio source across
                //AudioSource audioSource = gameObject.AddComponent<AudioSource>();
                AudioSource[] sources = GetComponents<AudioSource>();
                

                AudioSource _music = sources.Where(a => a.priority == 0).Single<AudioSource>();
                AudioSource _soundFX = sources.Where(a => a.priority == 1).Single<AudioSource>();
                _music.loop = true;
                AudioManager.Initialize(_music, _soundFX);
            }
            else
            {
                Destroy(gameObject);

            }
        }

       

        void Start()
        {
            EventManager.AddListener(EventName.Play, OnStartGame);
            EventManager.AddListener(EventName.Gameover, OnGameOver);
            EventManager.AddListener(EventName.SoundtrackEquipped, OnSoundtrackChanged);
            UpdateSoundtrack();
        }

        #region Events
        void OnStartGame(int unused)
        {
            AudioManager.PlayMusic(true);
        }

        void OnGameOver(int unused)
        {
            AudioManager.PlayMusic(false);
        }

        

        void OnSoundtrackChanged(int unused)
        {
            UpdateSoundtrack();
        }
        #endregion

        void UpdateSoundtrack()
        {
            AudioManager.SelectSoundtrack(GM._instance.SoundtrackEquipped);
        }
    }
    
}