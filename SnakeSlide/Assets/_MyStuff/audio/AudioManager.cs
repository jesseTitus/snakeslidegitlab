﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace SnakeSlide
{
    public static class AudioManager
    {
        public static bool Initialized { get; private set; }

        //static Dictionary<SoundEffect, AudioClip> _clips = new Dictionary<SoundEffect, AudioClip>();
        static AudioSource _music;
        static AudioSource _soundFX;
        

        public static void Initialize(AudioSource music, AudioSource soundFX)
        {

            _music = music;
            _soundFX = soundFX;
        }

        public static void PlaySound(SoundEffect sound)
        {
            if (AudioPreferences)
            {
                _soundFX.PlayOneShot(DataBase.soundEffects[sound]);
            }
        }

        public static void SelectSoundtrack(SoundTrack soundtrack)
        {
            _music.clip = DataBase.soundtrackDict[soundtrack].Clip;
        }


        public static void PlayMusic(bool on)
        {
            Assert.IsNotNull(_music, "Assertion failed: audiosource null!");
            
            if (on && AudioPreferences)
            {
                _music.Play();
            }
            else
            {
                _music.Stop();
            }
        }

        public static void ToggleAudio()
        {
            AudioPreferences = !AudioPreferences;
        }

        public static bool AudioPreferences
        {
            get
            {
                if (!PlayerPrefs.HasKey("AudioPreferences"))
                {
                    AudioPreferences = true;
                }
                return PlayerPrefs.GetInt("AudioPreferences") == 1;
            }
            private set
            {
                PlayerPrefs.SetInt("AudioPreferences", value == true? 1: 0);
                PlayerPrefs.Save();
            }
        }
    }
}