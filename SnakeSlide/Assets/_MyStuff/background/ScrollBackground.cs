﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class ScrollBackground : MonoBehaviour
    {

        public float scrollSpd;
        MeshRenderer meshRenderer;

        void Start()
        {
            meshRenderer = GetComponent<MeshRenderer>();
        }

        void Update()
        {
            //float y = Time.time * scrollSpd;

            Vector2 offset = new Vector2(0, transform.position.y);
            meshRenderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
        }
    } 
}
