using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SnakeSlide
{
    public class MakeTintMatchBackgroundColor : MonoBehaviour
    {
        Image image;
        float alpha;
        void Start()
        {
            image = GetComponent<Image>();
            alpha = image.color.a;
            EventManager.AddListener(EventName.BackgroundUpdated, OnBackgroundUpdatedEvent);
            UpdateVisuals();
        }

        // Set the tint to match the background selected
        void OnBackgroundUpdatedEvent(int unused)
        {
            UpdateVisuals();
        }

        void UpdateVisuals()
        {
            var matchingColor = DataBase.tileDict[GM._instance.BackgroundEquipped].TileColor;
            matchingColor.a = alpha;
            image.color = matchingColor;
        }
    }

}