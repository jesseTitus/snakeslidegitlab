using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollMenuBackground : MonoBehaviour
{

    Image image;
    
    void Awake()
    {
        image = GetComponent<Image>();        
    }

    void Update()
    {
        image.material.mainTextureOffset += new Vector2(-0.0001f, -0.0001f);        
    }

    void OnDisable()
    {
        image.material.mainTextureOffset = Vector2.zero;
    }
}
