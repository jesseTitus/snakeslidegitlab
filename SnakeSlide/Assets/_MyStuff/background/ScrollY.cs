using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace SnakeSlide {
    public class ScrollY : MonoBehaviour {
        [SerializeField]
        float scrollMatchSpeed = 0;

        Material bgMat;
        float posPrev;
        float pos;

        void Start () {
            var img = GetComponent<Image>();
            var sr = GetComponent<SpriteRenderer>();
            var mr = GetComponent<MeshRenderer>();


            bgMat = img? img.material: sr? sr.material: mr? mr.material: null;
            if (bgMat != null)
            {
                EventManager.AddTListener(EventName.SendCoords, OnReceivedCoordsUIEvent);
            }
        }
        
        void OnReceivedCoordsUIEvent(Transform t)
        {
            if (pos == 0)
            {
                pos = t.position.y;
            } else
            {
                posPrev = pos;
                pos = t.position.y;
            }
            var offset = pos - posPrev;
            bgMat.mainTextureOffset = new Vector2(0, t.position.y / scrollMatchSpeed);
            //bgMat.mainTextureOffset += Vector2.up * offset;
        }

        void OnDisable()
        {
            bgMat.mainTextureOffset = Vector2.zero;
        }
    }
}
