//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using System.IO;

//public class NativeShareScript : MonoBehaviour
//{
//    const string screenshotString = "screenshot.png";
//    const string intentString = "android.content.Intent";

//    public GameObject canvasShareObj;

//    bool isProcessing = false;  //currently processing and sharing screenshot
//    bool isFocused = false;

//    public void ShareButtonPress()
//    {
//        if (!isProcessing)
//        {
//            //canvasShareObj.SetActive(true);
//            StartCoroutine(ShareScreenShot());
//        }
//    }

//    IEnumerator ShareScreenShot()
//    {
//        isProcessing = true;

//        yield return new WaitForEndOfFrame();

//        //Application.CaptureScreenshot(screenshotString, 2);
//        //string destination = Path.Combine(Application.persistentDataPath, screenshotString);

//        //yield return new WaitForSecondsRealtime(0.3f);

//        //check if in platform (iOS, android)
//        // make sure this is correct for platform
//        if (!Application.isEditor)
//        {
//            AndroidJavaClass intentClass = new AndroidJavaClass(intentString);
//            AndroidJavaObject intentObject = new AndroidJavaObject(intentString);
//            intentObject.Call<AndroidJavaObject>("setAction", 
//                intentClass.GetStatic<string>("ACTION_SEND"));
//            AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
//            //AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>(
//            //    "parse", "file://" + destination);
//            //intentObject.Call<AndroidJavaObject>("putExtra",
//            //    intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
//            //intentObject.Call<AndroidJavaObject>("putExtra",
//            //    intentClass.GetStatic<string>("EXTRA_TEXT"), "CAN YOU BEAT MY SCORE?");
//            //intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");
//            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.UnityPlayer");
//            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
//            AndroidJavaObject chooser = intentClass.CallStatic<AndroidJavaObject>(
//                "createChooser", intentObject, "Share you new score");
//            currentActivity.Call("startActivity", chooser);

//            yield return new WaitForSecondsRealtime(1);
//        }

//        yield return new WaitUntil(() => isFocused);
//        canvasShareObj.SetActive(false);
//        isProcessing = false;
//    }

//    void OnApplicationFocus(bool focus)
//    {
//        isFocused = focus;
//    }
//}
