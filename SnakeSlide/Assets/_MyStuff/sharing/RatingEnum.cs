using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public enum RatingEnum
    {
        OneStar,
        TwoStars,
        ThreeStars,
        FourStars,
        FiveStars
    }
}
