using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class RateButton : MonoBehaviour, IButtonAction
    {
        [SerializeField]
        RatingEnum rating = RatingEnum.FiveStars;

        public RatingEnum Rating { get { return rating; } }
        public void Execute()
        {
        }
    }
}