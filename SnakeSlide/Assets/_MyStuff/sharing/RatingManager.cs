using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SnakeSlide
{
    public class RatingManager : MonoBehaviour
    {
        Text leftButton;
        Text rightButton;
        //Color originalStarColor;
        Sprite starOn;
        Sprite starOff;

        enum ReviewButton
        {
            LeftButton,
            RightButton
        }

        enum ReviewState {
            NothingClicked,
            LessThanFourClicked,
            FourOrGreaterClicked
        }
        ReviewState reviewState = ReviewState.NothingClicked;



        Dictionary<ReviewButton, string[]> reviewMessages = new Dictionary<ReviewButton, string[]>();
        Dictionary<RatingEnum, Image> stars = new Dictionary<RatingEnum, Image>();

        void Start()
        {
            starOn = DataBase.StarOn;
            starOff = DataBase.StarOff;

            //Time.timeScale = 0f;
            //leftButton = GameObject.FindGameObjectWithTag("LeftReviewButton").GetComponentInChildren<Text>();
            //rightButton = GameObject.FindGameObjectWithTag("RightReviewButton").GetComponentInChildren<Text>();

            foreach(var star in GameObject.FindGameObjectsWithTag("Star"))
            {
                // {1stars: Img0, 2stars:Img1, 3Stars:Img2..}
                stars.Add(star.GetComponent<Star>().rating, star.GetComponent<Image>());
            }
            //originalStarColor = stars[0].color;
            HighlightStars(4);

            // assign messages
            //reviewMessages[ReviewButton.LeftButton] = new string[] { "Never", "Cancel" };
            //reviewMessages[ReviewButton.RightButton] = new string[] { "Not now", "Submit", "Give Feedback?" };

            // initiate proper messages
            //leftButton.text = reviewMessages[ReviewButton.LeftButton][0];
            //rightButton.text = reviewMessages[ReviewButton.RightButton][0];
        }
        
        public void PauseForRating()
        {
            Time.timeScale = 0f;
        }

        void HighlightStars(int num)
        {
            for (var i = 0; i < 5; i++)
            {
                stars[(RatingEnum)i].sprite = starOff;
            }
            for (var i = 0; i <= num; i++)
            {
                stars[(RatingEnum)i].sprite = starOn;
            }
        }

        #region RateXStars
        public void Rate1Stars()
        {
            Rate(RatingEnum.OneStar);
        }
        public void Rate2Stars()
        {
            Rate(RatingEnum.TwoStars);
        }
        public void Rate3Stars()
        {
            Rate(RatingEnum.ThreeStars);
        }
        public void Rate4Stars()
        {
            Rate(RatingEnum.FourStars);
        }
        public void Rate5Stars()
        {
            Rate(RatingEnum.FiveStars);
        }
        #endregion

        void Rate(RatingEnum rating)
        {
            //highlight approariate number of stars
            HighlightStars((int)rating);

            if (rating > RatingEnum.ThreeStars)
            {
                //prompt buttons (cancel, submit)
                //leftButton.text = reviewMessages[ReviewButton.LeftButton][1];
                //rightButton.text = reviewMessages[ReviewButton.RightButton][1];
                reviewState = ReviewState.FourOrGreaterClicked;

            } else
            {
                // change buttons (cancel, submit feedback)
                //leftButton.text = reviewMessages[ReviewButton.LeftButton][1];
                //rightButton.text = reviewMessages[ReviewButton.RightButton][2];
                reviewState = ReviewState.LessThanFourClicked;
            }
        }
        
        public void OnRightButtonClicked()
        {
            SendUserToStore();
            PlayerPrefs.SetInt("ShowRating", 1);
            //switch (reviewState) {
            //    case ReviewState.NothingClicked:
            //        // 'Not Now' - back out of window
            //        CloseWindow();
            //        break;

            //    case ReviewState.LessThanFourClicked:
            //        // 'Give feedback?'
            //        OnSendFeedbackClicked();
            //        break;

            //    case ReviewState.FourOrGreaterClicked:
            //        // 'Submit'
            //        SendUserToStore();
            //        break;
            //}


            CloseWindow();
        }
        public void OnLeftButtonlicked()
        {
            if (reviewState == ReviewState.NothingClicked)
            {
                PlayerPrefs.SetInt("ShowRating", 1);
            }
            CloseWindow();
        }

        void CloseWindow()
        {
            Destroy(gameObject);
            Time.timeScale = 1f;
        }

        void OnSendFeedbackClicked()
        {
            // send user to email?
            Application.OpenURL(ConfigUtils.FeedbackUrl);
            CloseWindow();
        }

        void SendUserToStore()
        {
#if UNITY_ANDROID
            // YOUR_ID => package name (com.companyname.appname)
            //Application.OpenURL("market://details?id=YOUR_ID");
            Application.OpenURL(string.Format("market://details?id={0}", Application.identifier /*ConfigUtils.Id*/));
#elif UNITY_IPHONE
		    Application.OpenURL("itms-apps://itunes.apple.com/app/idYOUR_ID");
#endif
        }
    }
}