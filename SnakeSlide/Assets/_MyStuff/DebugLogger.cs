using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    /// <summary>
    /// Keeps all debug messages within editor to optimize performance
    /// </summary>
    public class DebugLogger : MonoBehaviour
    {
        public static DebugLogger _instance;    //singleton

        void Awake()
        {
            #region singleton
            if (_instance == null)
            {
                _instance = this;
            }
            else if (_instance != this)
            {
                Destroy(this);
            }
            #endregion
        }

        public void Log(string debugMessage)
        {
#if UNITY_EDITOR
            Debug.Log(debugMessage);
#endif
        }
    } 
}
