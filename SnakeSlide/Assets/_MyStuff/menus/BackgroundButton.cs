﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SnakeSlide
{
    public class BackgroundButton : ShopItem
    {
        public TileType myTileType;            //grass, sand, water


        #region implement abstract properties
        
        protected override Sprite ItemBg { get { return DataBase.tileDict[myTileType].ShopSprite; } }   //don't need, use icon
        protected override int ItemCost { get { return DataBase.tileDict[myTileType].Cost; } }
        protected override int ItemNumber
        {
            get { return (int)myTileType + (int)Item.Bg1; }
        }
        protected override bool IsItemOwned
        {
            get { return GM._instance.BackgroundsOwned.Contains(myTileType); }
        }
        protected override bool IsItemEquipped
        {
            get { return GM._instance.BackgroundEquipped == myTileType; }
        }
        protected override Color BgColor { get { return Color.white; } }
        #endregion

        #region properties
        //protected override Image.Type IconImageType { get {return Image.Type.Tiled; } }
        #endregion


        public override void Start()
        {
            //Invoke
            unityEvents[EventName.BackgroundPurchased] = new BackgroundPurchasedEvent();
            unityEvents[EventName.BackgroundEquipped] = new BackgroundEquippedEvent();
            EventManager.AddInvoker(EventName.BackgroundPurchased, this);
            EventManager.AddInvoker(EventName.BackgroundEquipped, this);

            //Listen
            EventManager.AddListener(EventName.BackgroundEquipped, OnBackgroundEquipped);

            base.Start();
            UpdateVisuals();
            initialized = true;
        }

        #region methods
        void OnBackgroundEquipped(int unused)
        {
            UpdateVisuals();
        }
        
        protected override void AttemptPurchase()
        {
            unityEvents[EventName.BackgroundPurchased].Invoke((int)myTileType);
        }

        protected override void EquipSelectedItem()
        {
            unityEvents[EventName.BackgroundEquipped].Invoke((int)myTileType);
        }

        #endregion
    }
}
