using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class ExitShopButton : MonoBehaviour, IButtonAction
    {
        GameObject ShopPanel;

        void Start()
        {
            ShopPanel = GameObject.FindGameObjectWithTag("ShopPanel");
        }

        public void Execute()
        {
            ShopPanel.SetActive(false);
        }
    }

}