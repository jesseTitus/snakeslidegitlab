﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    interface IButtonAction
    {

        void Execute();

    }
}
