﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class TrophyButton : MonoBehaviour, IButtonAction
    {
        public void Execute()
        {
            //Debug.Log("Trophy button pressed");
            DebugLogger._instance.Log("Trophy button pressed");
        }
    }

}