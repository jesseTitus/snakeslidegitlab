using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class SendToMicrosPanel : MonoBehaviour, IButtonAction
    {
        GameObject MicrosPanel;

        void Start()
        {
            MicrosPanel = GameObject.FindGameObjectWithTag("MicrosPanel");
        }

        public void Execute()
        {
            MicrosPanel.SetActive(true);
        }
    }
}
