﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class BackButton : MonoBehaviour
    {

        public void Action()
        {
            GM._instance.State = GameState.Gameover;
        }
    } 
}
