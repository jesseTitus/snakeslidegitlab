﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class RestartButton : MonoBehaviour, IButtonAction
    {
        public void Execute()
        {
            //AudioManager.PlaySound(SoundEffect.PositiveSound);
            Invoke("Restart", 0f);  //TODO - add delay + fade?
        }

        void Restart()
        {
            GM._instance.State = GameState.Restart;
        }
    }
}