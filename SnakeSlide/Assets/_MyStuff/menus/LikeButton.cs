﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakeSlide
{
    public class LikeButton : MonoBehaviour, IButtonAction
    {
        //public const string facebookURL = "https://www.facebook.com/Upfire-1981247978846757/";
        public const string playstoreURL = "https://play.google.com/store/apps/details?id=com.AwakenedSoftware.Upsnake";
        public void Execute()
        {
            //Application.OpenURL(facebookURL);
            Application.OpenURL(playstoreURL);
        }
    } 
}
