﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SnakeSlide
{
    public class SoundButton : MonoBehaviour, IButtonAction
    {
        Sprite soundOn;
        Sprite soundOff;
        Image _renderer;
        void Start()
        {
            _renderer = GetComponent<Image>();
            //soundOn = DataBase.audioPrefSprites[true];
            //soundOff = DataBase.audioPrefSprites[false];
            UpdateButtonDisplay();
        }

        public void Execute()
        {
            AudioManager.ToggleAudio();
            UpdateButtonDisplay();
        }

        void UpdateButtonDisplay()
        {
            _renderer.sprite = DataBase.audioPrefSprites[AudioManager.AudioPreferences];
            //if (AudioManager.AudioPreferences)
            //{
            //    _renderer.sprite = soundOn;
            //}
            //else
            //{
            //    _renderer.sprite = soundOff;
            //}
        }
    }

}