﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SnakeSlide
{
    public class SkinButton : ShopItem
    {
        
        public Skin mySkin;         // e.g. - SKIN.SNAKE

        #region implement abstract properties

        protected override Sprite Icon { get { return SkinDatabase.skinDictionary[mySkin].Head; } }
        protected override Sprite ItemBg { get { return SkinDatabase.snakeShopBg; } }
        protected override int ItemCost { get { return SkinDatabase.skinDictionary[mySkin].Cost; } }
        protected override int ItemNumber { get { return (int)mySkin; } }
        protected override bool IsItemOwned
        {
            get {return GM._instance.SkinsOwned.Contains(mySkin);}
        }
        protected override bool IsItemEquipped
        {
            get { return GM._instance.SkinEquipped == mySkin; }
        }
        protected override Color BgColor { get { return DataBase.shopSnakeBgColor; } }
        #endregion

        #region properties
        protected override bool HasIcon { get { return true; } }

        #endregion


        public override void Start()
        {
            base.Start();


            // Invoke
            unityEvents[EventName.SkinEquipped] = new EquippedSkinEvent();
            unityEvents[EventName.SkinPurchased] = new BoughtSkinEvent();
            EventManager.AddInvoker(EventName.SkinEquipped, this);
            EventManager.AddInvoker(EventName.SkinPurchased, this);

            // Listen
            EventManager.AddListener(EventName.SkinEquipped, OnSkinChanged);
            
            initialized = true;
            UpdateVisuals();


        }

        #region methods

        void OnSkinChanged(int unused)
        {
            UpdateVisuals();
        }
        

        protected override void AttemptPurchase()
        {
            unityEvents[EventName.SkinPurchased].Invoke((int)mySkin);
        }

        protected override void EquipSelectedItem()
        {
            unityEvents[EventName.SkinEquipped].Invoke((int)mySkin);
        }
        #endregion
    } 
}
