﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace SnakeSlide
{
    public class GemsShopText : MonoBehaviour
    {
        TextMeshProUGUI myText;
        [SerializeField]
        bool showOldScorePlusCollected = false;         // (Gems: xx + x)

        void Awake()
        {
            myText = GetComponent<TextMeshProUGUI>();
        }

        void Start()
        {
            EventManager.AddListener(EventName.GemsSpent, OnGemsSpent);
            UpdateGemCountText();
        }

        void OnGemsSpent(int unused)
        {
            UpdateGemCountText();
        }

        void UpdateGemCountText()
        {
            if (GM._instance)
            {
                var collectedGemsThisGame = GM._instance.Gems - GM._instance.StartGems;
                if (showOldScorePlusCollected && collectedGemsThisGame > 0 && GM._instance.StartGems > 0)
                {
                    // Show old gem score + num collected this game
                    myText.text = GM._instance.StartGems.ToString() + " + " + collectedGemsThisGame.ToString();
                }
                else
                {
                    // Just showing current num of gems
                    myText.text = GM._instance.Gems.ToString();
                }
            }
        }
    } 
}
