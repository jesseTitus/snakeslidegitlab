﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SnakeSlide
{
    public class RemoveAdsButton : MonoBehaviour, IButtonAction
    {
        GameObject MicrosPanel;

        void Start()
        {
            MicrosPanel = GameObject.FindGameObjectWithTag("MicrosPanel");
        }

        public void Execute()
        {
            //Debug.Log("Prompt player to remove ads");
            DebugLogger._instance.Log("Prompt player to remove ads");
            MicrosPanel.SetActive(true);
        }
    } 
}
