using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace SnakeSlide
{
    public class UIManager : MonoBehaviour
    {
        #region fields
        [SerializeField]
        GameObject GameoverPanel = null;
        [SerializeField]
        GameObject ScorePanel = null;
        [SerializeField]
        GameObject ShopPanel = null;
        [SerializeField]
        GameObject RatePanel = null;
        //[SerializeField]
        //GameObject ShopBoxConfirmationPanel = null;


        [SerializeField]
        TextMeshProUGUI InGameScoretext = null;
        [SerializeField]
        TextMeshProUGUI FinalScoreText = null;
        [SerializeField]
        TextMeshProUGUI HighscoreText = null;
        //[SerializeField]
        //TextMeshProUGUI GemsShopText = null;

        #endregion

        void Start()
        {
            EventManager.AddListener(EventName.Load, OnLoadEvent);
            EventManager.AddListener(EventName.Gameover, OnGameOverEvent);
            EventManager.AddListener(EventName.Restart, OnRestartEvent);
            EventManager.AddListener(EventName.Play, OnPlayEvent);
            EventManager.AddListener(EventName.Rate, OnRateEvent);
            EventManager.AddListener(EventName.PointCollected, OnPointsCollectedEvent);

            ResetPanels();
            ScorePanel.SetActive(true);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (ShopPanel.activeSelf)
                {
                    OpenMainMenu();
                }
            }
        }

        #region BUTTONs
        public void OpenMainMenu()
        {
            ResetPanels();
            GameoverPanel.SetActive(true);
        }

        public void OpenShop()
        {
            ResetPanels();
            ShopPanel.SetActive(true);
        }
        
        
        #endregion

        void ResetPanels()
        {
            GameoverPanel.SetActive(false);
            ShopPanel.SetActive(false);
            RatePanel.SetActive(false);
            ScorePanel.SetActive(false);
        }

        #region EVENTS
        void OnLoadEvent(int unused)
        {
            ResetPanels();
            ScorePanel.SetActive(true);
        }

        void OnGameOverEvent(int unused)
        {
            ResetPanels();
            //Debug.Log("ShowTutorial: " + PlayerPrefs.GetInt("ShowTutorial"));
            //Debug.Log("Rate: " + PlayerPrefs.GetInt("ShowRating"));
            if (PlayerPrefs.GetInt("ShowTutorial") == 1 &&
                PlayerPrefs.GetInt("ShowRating") == 0 &&
                GM._instance.Gems >= 15 &&
                GM._instance.Gems % 3 == 0)
            {
                // finished tutorial, never rated, and just finished match? rate prompt
                RatePanel.SetActive(true);
                StartCoroutine("WaitForRate");
            }
            else
            {

                GameoverPanel.SetActive(true);
                FinalScoreText.text = GM._instance.Points.ToString();
                HighscoreText.text = GM._instance.Highscore.ToString();
                //TODO - show gems: old total + score
            }
        }

        void OnRestartEvent(int unused)
        {
            ResetPanels();
            ScorePanel.SetActive(true);
        }

        void OnPlayEvent(int unused)
        {
            ResetPanels();
            ScorePanel.SetActive(true);
        }

        void OnRateEvent(int unused)
        {
            ResetPanels();
            RatePanel.SetActive(true);
            RatePanel.GetComponent<RatingManager>().PauseForRating();
        }

        void OnPointsCollectedEvent(int unused)
        {
            //StartCoroutine("UpdateScoreEffect");
            InGameScoretext.text = GM._instance.Points.ToString();
        }
        

        

        #endregion

        IEnumerator UpdateScoreEffect()
        {
            var txt = InGameScoretext.rectTransform;
            var incr = 0.05f;

            while (txt.pivot.x < 1f)
            {
                txt.pivot = new Vector2(txt.pivot.x + incr, txt.pivot.y);
                yield return null;
            }
            txt.pivot = new Vector2(0, txt.pivot.y);
            while (txt.pivot.x < 0.5f) {
                txt.pivot = new Vector2(txt.pivot.x + incr, txt.pivot.y);
                yield return null;
            }
            InGameScoretext.text = GM._instance.Points.ToString();
            StopCoroutine("UpdateScoreEffect");
        }

        IEnumerator WaitForRate()
        {
            while (PlayerPrefs.GetInt("ShowRating") == 0)
            {
                yield return null;
            }

            OnGameOverEvent(-1);
        }
    }
}
