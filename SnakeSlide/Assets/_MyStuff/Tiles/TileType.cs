using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SnakeSlide
{
    public enum TileType
    {
        Grass,
        Sand,
        WaterDark,
        WaterLight
    }
}
